# SNEAKY
![build status](https://ci.atommac.com/api/badges/atomdmac/sneaky/status.svg)

## A rogue-like game focusing on stealth
* Time only moves when you do
* Resource Management: What you carry presents trade-offs (effective weapon vs. slow to carry and use, limited carrying capacity: 2-4 items?)
