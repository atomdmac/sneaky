import * as ROT from 'rot-js';

export const Scheduler = new ROT.Scheduler.Speed();
export const Engine = new ROT.Engine(Scheduler);
