import * as ROT from 'rot-js';
import { randomItem } from './helpers/array';

export interface Cell {
  x: number;
  y: number;
  passable: boolean;
}

export class Floor {
  public cells: Cell[];
  public spawns: Cell[];
  public doors: Cell[];
  public deadEnds: Cell[];

  public spawnNeighborCounts = [];

  constructor (private width = 30, private height = 30) {
    this.cells = this.createDungeon(width, height);
    this.spawns = this.findWithPassableNeighbors(3);
    this.deadEnds = this.findWithPassableNeighbors(1, 1)
    this.doors = [this.deadEnds[0], this.deadEnds[this.deadEnds.length - 1]];
  }

  createDungeon (width: number, height: number): Cell[] {
    const cells: Cell[] = [];
    const dungeon = new ROT.Map.Uniform(
      width, height, {
        roomWidth: [6, 6],
        roomHeight: [10, 10],
        roomDugPercentage: 100,
        timeLimit: 5000
      }
    );

    const rogue = new ROT.Map.Rogue(width, height, {
      roomWidth: [6, 6],
      roomHeight: [10, 10],
    });
    const roguePassable = {};
    rogue.create((x: number, y: number, contents: number) => {
      roguePassable[`${x}_${y}`] = contents;
    })

    dungeon.create((y, x, passable) => {
      cells.push({
        x: x,
        y: y,
        passable: !passable || !roguePassable[`${x}_${y}`]
      });
    });
    return cells;
  }

  createMaze (width: number, height: number): Cell[] {
    const cells:Cell[] = [];
    var dm = new ROT.Map.DividedMaze(this.width, this.height);
    dm.create((y, x, passable) => {
      cells.push({
        x: x,
        y: y,
        passable: !passable
      });
    });
    return cells;
  }

  findWithPassableNeighbors (min = 1, max = 8) {
    return this.cells.filter(c => {
      if (!c.passable) return false;
      const neighbors = this.getPassableNeighbors(c.x, c.y);
      return neighbors.length >= min && neighbors.length <= max;
    })
  }

  findSpawnPoints (minPassableNeighbors = 8) {
    return this.cells.filter(c => (
      c.passable && this.getPassableNeighbors(c.x, c.y).length >= minPassableNeighbors
    ));
  }

  getRandomSpawn (): Cell {
    return randomItem(this.spawns);
  }

  getNeighbors (x, y) {
    const neighbors = [];
    for(let _x = -1; _x<2; _x++) {
      for(let _y = -1; _y<2; _y++) {
        if (_x === 0 && _y === 0) continue;
        const cell = this.getCell(x + _x, y +_y, true);
        if (cell) neighbors.push(cell);
      }
    }
    return neighbors;
  }

  getPassableNeighbors (x, y) {
    return this.getNeighbors(x, y).filter(c => c.passable);
  }

  getCell (x, y, noBounderies = false) {
    if (x < 0 || y < 0 || x >= this.width || y >= this.height) {
      if (noBounderies) {
        return null;
      } else {
        throw new Error('Coordinates are out of floor bounderies.');
      }
    }
    return  this.cells[y * (this.width) + x];
  }
}
