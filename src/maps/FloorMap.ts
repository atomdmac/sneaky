import Phaser from 'phaser';
import { DEPTHS } from '@src/config';
import SneakyScene from '@src/scenes/i-sneaky-scene';
import { AStarFinder } from 'astar-typescript';

export const LayerNames = {
  FLOOR: 'floor',
  FIELD_OF_VIEW: 'fieldOfView',
};

export default function createMap(scene: SneakyScene, mapKey: string) {
  const tileMap = scene.add.tilemap(mapKey);

  const tiles = [
    tileMap.addTilesetImage('dirt_1.png'),
    tileMap.addTilesetImage('stone_1.png'),
  ];

  // Floor Layer
  const floor = tileMap.createLayer(LayerNames.FLOOR, tiles, 0, 0);

  // Field of View Layer
  const fieldOfView = tileMap.createBlankLayer(
    LayerNames.FIELD_OF_VIEW,
    tiles,
    0,
    0,
    tileMap.width * 2,
    tileMap.height * 2,
    32,
    32
  );
  fieldOfView.getTilesWithinShape(fieldOfView.getBounds()).forEach((t: any) => {
    const tile = fieldOfView.putTileAt(1, t.x, t.y);
    tile.tint = 0x000000;
    tile.setAlpha(0);
  });
  fieldOfView.setDepth(DEPTHS.fieldOfView);

  // Doors Layer
  const doors = scene.physics.add.group();
  doors.defaults =
    {} as unknown as Phaser.Types.Physics.Arcade.PhysicsGroupDefaults;
  tileMap.createLayer('doors', ['door'], 0, 0);
  tileMap.getObjectLayer('doors').objects.map((d) => {
    if (d?.x && d?.y && d?.width && d?.height) {
      doors.add(
        scene.add.sprite(d.x + d.width / 2, d.y + d.height / 2, 'door')
      );
    }
  });

  // Make the floor layer the selected layer by default..
  tileMap.setLayer(LayerNames.FLOOR);

  function initMatrix(
    layer: Phaser.Tilemaps.TilemapLayer,
    width: number,
    height: number
  ): number[][] {
    const matrix: number[][] = [];
    for (let y = 0; y < height; y++) {
      matrix[y] = [];
      for (let x = 0; x < width; x++) {
        const tile = layer.getTileAt(x, y);
        matrix[y][x] = tile.index - 1;
      }
    }
    return matrix;
  }

  const matrix = initMatrix(floor, tileMap.width, tileMap.height);

  const astar = new AStarFinder({
    grid: {
      matrix,
    },
    heuristic: 'Manhatten',
    diagonalAllowed: false,
    weight: 1,
  });

  return {
    tileMap,
    doors,
    astar,
  };
}
