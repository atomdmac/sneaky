import { Tilemaps } from 'phaser';

export function getRandomSpawnPoints(
  map: Tilemaps.Tilemap,
  numberOfSpawns = 1
): { x: number; y: number }[] {
  const chosen = [];
  while (chosen.length < numberOfSpawns) {
    const candidate = {
      x: Math.floor(Math.random() * map.width),
      y: Math.floor(Math.random() * map.height),
    };

    const tile = map.getTileAt(candidate.x, candidate.y);
    if (!tile?.collides) {
      chosen.push(candidate);
    }
  }
  return chosen;
}
