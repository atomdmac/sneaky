export type RoomConfig = {
  x: number;
  y: number;
  width: number;
  height: number;
  minWidth: number;
  minHeight: number;
  splitThreshold: number;
};

const DefaultRoomConfig: RoomConfig = {
  x: 0,
  y: 0,
  width: 10,
  height: 10,
  minWidth: 5,
  minHeight: 5,
  splitThreshold: 0.75,
};

export type Door = {
  x: number;
  y: number;
  state: 'open' | 'closed' | 'broken';
};

export class BisectionRoom {
  public config: RoomConfig;
  public childRooms: BisectionRoom[];
  public doors: Door[];

  constructor(config: Partial<RoomConfig> = DefaultRoomConfig) {
    this.config = {
      ...DefaultRoomConfig,
      ...config,
    };
    this.childRooms = [];
    this.doors = [];
  }

  public getRandomSplitPoint(
    useWidth = true,
    noBounds = false,
    illegalValues: number[] = [],
    attempts = 0
  ): number {
    const { width, height, minWidth, minHeight } = this.config;

    const dimension = useWidth ? width : height;
    let min = useWidth ? minWidth : minHeight;
    let max = useWidth ? width - minWidth : height - minHeight;

    if (noBounds) {
      min += useWidth ? -minWidth : -minHeight;
      max += useWidth ? -minWidth : -minHeight;
    }

    const half = Math.round(dimension / 2);

    if (max < min || min < 0 || max < 0) return half;

    const rand = min + Math.random() * (max - min);
    let split = Math.round(rand); // split should always be at least 1

    if (
      illegalValues.includes(split) &&
      // Prevent endless loops
      attempts < 10
    ) {
      // Nudge away from corners.
      if (split === 0) {
        split += 1;
      } else if (split === dimension) {
        split -= 1;
      } else {
        if (attempts >= 10) {
          console.log('FAILED TO FIND SPLIT');
          return half;
        }
        return this.getRandomSplitPoint(
          useWidth,
          noBounds,
          illegalValues,
          attempts + 1
        );
      }
    }

    return split;
  }

  public split(flag: boolean | null = null): BisectionRoom[] {
    if (this.childRooms.length) this.childRooms;

    const { x, y, height, width, minWidth, minHeight } = this.config;

    if (width <= minWidth && height <= minHeight) {
      this.childRooms;
    }

    const vertical = flag !== null ? flag : width > height;

    // Divide vertically
    if (vertical) {
      const split = this.getRandomSplitPoint(true);

      if (split <= minWidth) return this.childRooms;

      this.childRooms = [
        new BisectionRoom({
          ...this.config,
          x,
          y,
          width: split,
          height,
        }),
        new BisectionRoom({
          ...this.config,
          x: x + split,
          y,
          width: width - split,
          height,
        }),
      ];
    } else {
      const split = this.getRandomSplitPoint(false);

      if (split <= minHeight) return this.childRooms;

      this.childRooms = [
        new BisectionRoom({
          ...this.config,
          x,
          y,
          width,
          height: split,
        }),
        new BisectionRoom({
          ...this.config,
          x,
          y: y + split,
          width,
          height: height - split,
        }),
      ];
    }

    // Create grandchildren rooms
    return this.childRooms.map((cr) => cr.split()).flat();
  }

  public generateDoors() {
    if (!this.childRooms.length) return;
    const [room1, room2] = this.childRooms;

    // Is horizontal split
    if (room1.config.x === room2.config.x) {
      this.doors.push({
        x: room1.config.x + Math.ceil(room1.config.height / 2),
        y: room2.config.y,
        // x: room1.config.x + 2,
        // y: room2.config.y,
        state: Math.random() > 0.5 ? 'open' : 'closed',
      });

      this.doors.push({
        x: room1.config.x + Math.ceil(room1.config.height / 2),
        y: room2.config.y,
        state: Math.random() > 0.5 ? 'open' : 'closed',
      });
    }

    // Is vertical split
    if (room1.config.y === room2.config.y) {
      this.doors.push({
        x: room2.config.x,
        y: room1.config.y + Math.ceil(room1.config.height / 2),
        // x: room2.config.x,
        // y: room1.config.y + 2,
        state: Math.random() > 0.5 ? 'open' : 'closed',
      });
      this.doors.push({
        x: room2.config.x,
        y: room1.config.y + Math.ceil(room1.config.height / 2),
        state: Math.random() > 0.5 ? 'open' : 'closed',
      });
    }

    this.childRooms.forEach((cr) => cr.generateDoors());
  }

  public generateDoorsV2(): Door[] {
    if (!this.childRooms.length) return [];

    const [room1, room2] = this.childRooms;
    // const [, room1Child2] = room1.childRooms;
    // const [, room2Child2] = room2.childRooms;

    // Horizontal split
    if (room1.config.x === room2.config.x) {
      console.log('H');
      this.doors.push({
        x: room2.config.x + this.getRandomSplitPoint(true, true),
        y: room2.config.y,
        state: 'open',
      });
    }

    // Vertical split
    if (room1.config.y === room2.config.y) {
      console.log('V');
      this.doors.push({
        x: room2.config.x,
        y: room2.config.y + this.getRandomSplitPoint(false, true),
        state: 'open',
      });
    }

    // Not totally sure why this is necessary but doors are always offset by
    // 1 on both axes :shrug:
    this.doors.forEach((d) => {
      d.x -= 1;
      d.y -= 1;
    });

    return this.childRooms
      .map((cr) => cr.generateDoorsV2())
      .flat()
      .concat(this.doors);
  }

  public generateDoorsV3(): Door[] {
    if (this.childRooms.length) {
      return this.childRooms.map((cr) => cr.generateDoorsV3()).flat();
    }

    const { x, y, width, height } = this.config;

    if (x !== 0) {
      this.doors.push({
        x: x - 1,
        y: y + this.getRandomSplitPoint(false),
        state: 'closed',
      });
    }

    // TODO: Pull in map width dynamically
    if (x + width === 29) {
      this.doors.push({
        x: x + width - 1,
        y: y + this.getRandomSplitPoint(false),
        state: 'closed',
      });
    }

    if (y !== 0) {
      this.doors.push({
        x: x + this.getRandomSplitPoint(true),
        y: y - 1,
        state: 'closed',
      });
    }

    // TODO: Pull in map height dynamically
    if (y + height === 29) {
      this.doors.push({
        x: x + this.getRandomSplitPoint(true),
        y: y + height - 1,
        state: 'closed',
      });
    }

    this.doors.forEach((d) => ({
      ...d,
      x: d.x - 1,
      y: d.y - 1,
    }));

    return this.doors;
  }

  public draw(scene: Phaser.Scene) {
    const { x, y, width, height } = this.config;
    const rect = scene.add.rectangle(x, y, width, height, 0xff0000);
    rect.setOrigin(0, 0);
    rect.setAlpha(0.05);
    rect.setStrokeStyle(1, 0xffffff);

    this.childRooms.forEach((c) => {
      c.draw(scene);
    });

    this.doors.forEach((d) => {
      const rect = scene.add.rectangle(d.x, d.y, 5, 5, 0xff00ff);
      rect.setAlpha(1);
    });
  }

  public toRectangle(): Phaser.Geom.Rectangle {
    const { x, y, width, height } = this.config;
    return new Phaser.Geom.Rectangle(x, y, width, height);
  }

  public populateTileMapLayer(terrainLayer: Phaser.Tilemaps.TilemapLayer) {
    terrainLayer.setCollision(1);
    // Draw rooms
    this.childRooms.forEach((cr) => {
      const rect = cr.toRectangle();

      for (let x = rect.x; x < rect.x + rect.width; x++) {
        terrainLayer.putTilesAt([1], x, 0);
        terrainLayer.putTilesAt([1], x, rect.y + rect.height - 1);
      }
      for (let y = rect.y; y < rect.y + rect.height; y++) {
        terrainLayer.putTilesAt([1], 0, y);
        terrainLayer.putTilesAt([1], rect.x + rect.width - 1, y);
      }

      cr.populateTileMapLayer(terrainLayer);
    });

    // Draw doors
    this.doors.forEach((d) => {
      // terrainLayer.putTileAt(2, d.x, d.y);
      terrainLayer.putTileAt(0, d.x, d.y);
    });
  }
}
