import config from '@src/config';
import { Math as PMath } from 'phaser';
import { Entity } from '@src/entities/entity';

export function lineCollidesTileMap(
  line: Phaser.Geom.Line,
  mapLayer: Phaser.Tilemaps.TilemapLayer,
  results: Array<PMath.Vector2> = []
): boolean {
  const points = Phaser.Geom.Line.BresenhamPoints(
    line,
    config.tiles.width,
    results
  );

  return !!points?.some(({ x = 0, y = 0 }) => {
    const tile = mapLayer.getTileAtWorldXY(x, y);
    return tile?.canCollide;
  });
}

export function hasLineOfSight(
  v1: PMath.Vector2,
  v2: PMath.Vector2,
  map: Phaser.Tilemaps.Tilemap,
  results: Array<PMath.Vector2> = []
): boolean {
  const halfWidth = map.tileWidth / 2;
  const halfHeight = map.tileHeight / 2;
  const collisionLayer = map.getLayer(0).tilemapLayer;
  const los = new Phaser.Geom.Line(
    v1.x + halfWidth,
    v1.y + halfHeight,
    v2.x + halfWidth,
    v2.y + halfHeight
  );
  if (lineCollidesTileMap(los, collisionLayer, results)) {
    return false;
  } else {
    return true;
  }
}

// A reusable vector object to use in calculations within isWithinFieldOfView.
const _angleVector = new PMath.Vector2();
const _target = new PMath.Vector2();
const _origin = new PMath.Vector2();

export function isWithinFieldOfView(
  origin: PMath.Vector2,
  bearing: PMath.Vector2,
  fieldOfViewRadians: number,
  target: PMath.Vector2,
  offsetX = 0,
  offsetY = 0
): boolean {
  _origin.setTo(origin.x + offsetX, origin.y + offsetY);
  _target.setTo(target.x + offsetX, target.y + offsetY);

  const actualAngle = _angleVector
    .setFromObject(_origin)
    .subtract(_target)
    .angle();
  const bearingAngle = _angleVector.setFromObject(bearing).negate().angle();
  const diff = Math.abs(bearingAngle - actualAngle);
  if (diff < fieldOfViewRadians / 2) {
    return true;
  } else {
    return false;
  }
}

export function canSeeTarget(source: Entity, target: Entity): boolean {
  const isWithinRange =
    source.body.position.distance(target) < source.sight.distance;
  return (
    isWithinRange &&
    hasLineOfSight(
      source.body.position,
      target.body.position,
      source.scene.map
    ) &&
    isWithinFieldOfView(
      source.body.position,
      source.direction.bearing,
      source.sight.fieldOfViewRadians,
      target.body.position
    )
  );
}
