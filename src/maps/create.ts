import config from '@src/config';
import ISneakyScene from '@src/scenes/i-sneaky-scene';

export function createMapData({
  width,
  height,
}: {
  width: number;
  height: number;
}): number[][] {
  const output: number[][] = [];
  for (let w = 0; w < width; w++) {
    output[w] = [];
    for (let h = 0; h < height; h++) {
      const index = Math.round(Math.random());
      output[w][h] = index;
    }
  }
  return output;
}

export function createDebugMap(scene: ISneakyScene) {
  const tilemapDimensions = {
    width: 100,
    height: 100,
  };
  const tileMap = scene.make.tilemap({
    data: createMapData(tilemapDimensions),
    tileWidth: config.tiles.width,
    tileHeight: config.tiles.height,
  });
  const tileSets = [
    tileMap.addTilesetImage(
      'dirt',
      'dirt',
      config.tiles.width,
      config.tiles.height,
      0,
      0
    ),
    tileMap.addTilesetImage(
      'stone',
      'stone',
      config.tiles.width,
      config.tiles.height,
      0,
      1
    ),
  ];
  const tileLayers = [
    tileMap.createLayer(0, tileSets[0]),
    tileMap.createLayer(1, tileSets[1]),
  ];

  return {
    tileMap,
    tileSets,
    tileLayers,
  };
}
