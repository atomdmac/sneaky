import { DEPTHS } from '@src/config';
import SneakyScene from '@src/scenes/i-sneaky-scene';
import { AStarFinder } from 'astar-typescript';
import Phaser from 'phaser';
import { BisectionRoom } from './bisection-map';

export const LayerNames = {
  FLOOR: 'floor',
  FIELD_OF_VIEW: 'fieldOfView',
};

export default function createMap(scene: SneakyScene, mapKey: string) {
  const bMap = new BisectionRoom({
    x: 0,
    y: 0,
    width: 30,
    height: 30,
    minWidth: 5,
    minHeight: 5,
  });
  bMap.split();
  bMap.generateDoorsV3();

  const tileMap = scene.add.tilemap(mapKey, 32, 32, 30, 30);
  const dirt = tileMap.addTilesetImage(
    'dirt_1.png',
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    0
  );
  const stone = tileMap.addTilesetImage(
    'stone_1.png',
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    1
  );
  const door = tileMap.addTilesetImage(
    'door',
    undefined,
    undefined,
    undefined,
    undefined,
    undefined,
    2
  );
  const tiles = [dirt, stone, door];

  const floor = tileMap.createBlankLayer(LayerNames.FLOOR, tiles);
  floor.setCollision(1);
  floor.fill(0);
  bMap.populateTileMapLayer(floor);

  // Field of View Layer
  const fieldOfView = tileMap.createBlankLayer(
    LayerNames.FIELD_OF_VIEW,
    tiles,
    0,
    0,
    tileMap.width * 2,
    tileMap.height * 2,
    32,
    32
  );
  fieldOfView.forEachTile((tile) => {
    tile.index = 0;
    tile.tint = 0x000000;
    tile.setAlpha(0);
  });
  fieldOfView.setDepth(DEPTHS.fieldOfView);
  fieldOfView.setVisible(true);

  // Doors Layer
  const doors = scene.physics.add.group();

  // Make the floor layer the selected layer by default..
  tileMap.setLayer(LayerNames.FLOOR);

  function initMatrix(
    layer: Phaser.Tilemaps.TilemapLayer,
    width: number,
    height: number
  ): number[][] {
    const matrix: number[][] = [];
    for (let y = 0; y < height; y++) {
      matrix[y] = [];
      for (let x = 0; x < width; x++) {
        const tile = layer.getTileAt(x, y);
        matrix[y][x] = tile.collides ? 1 : 0;
      }
    }
    return matrix;
  }

  const matrix = initMatrix(floor, tileMap.width, tileMap.height);

  const astar = new AStarFinder({
    grid: {
      matrix,
    },
    heuristic: 'Manhatten',
    diagonalAllowed: false,
    weight: 1,
  });

  return {
    tileMap,
    doors,
    astar,
  };
}
