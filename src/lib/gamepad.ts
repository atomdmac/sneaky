import { Scene } from 'phaser';

export function gamepadButtonDown(scene: Scene, button = 0): boolean {
  if (scene.input.gamepad.total === 0) return false;

  const pad = scene.input.gamepad.getPad(0);
  if (pad.isButtonDown(button)) {
    return true;
  }
  return false;
}
