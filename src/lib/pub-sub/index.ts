type AnyFunction = (...args: any[]) => any;

export default class PubSub<Fn extends AnyFunction = AnyFunction> {
  private _listeners: Map<Fn, Fn>;

  constructor() {
    this._listeners = new Map();
  }

  public on(fn: Fn) {
    this._listeners.set(fn, fn);
  }

  public off(fn: Fn) {
    this._listeners.delete(fn);
  }

  public clear() {
    this._listeners.clear();
  }

  public notify(...args: Parameters<Fn>) {
    this._listeners.forEach((l) => l(...args));
  }
}
