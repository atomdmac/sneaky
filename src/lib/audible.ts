import { DEPTHS } from '@src/config';
import { Entity } from '@src/entities/entity';
import { Scene, GameObjects, Math as PMath, Physics } from 'phaser';

export function createAudibleAnimation({
  x,
  y,
  width,
  height,
  color,
  scene,
}: {
  x: number;
  y: number;
  width: number;
  height: number;
  color: number;
  scene: Phaser.Scene;
}) {
  for (let i = 0; i < 3; i++) {
    const circ = scene.add.ellipse(x, y, width, height, color, 0);
    circ.setDepth(DEPTHS.audibles);
    circ.setStrokeStyle(1, 0xff0000, 1);
    circ.setAlpha(0);
    circ.setScale(0);

    const circTimeline = scene.tweens.timeline({});

    circTimeline.add({
      targets: circ,
      props: {
        alpha: 1,
        scale: 2,
      },
      duration: 250,
      delay: i * 250,
    });
    circTimeline.add({
      targets: circ,
      props: {
        alpha: 0,
        scale: 4,
      },
      duration: 1000,
      ease: Phaser.Math.Easing.Cubic.Out,
      onComplete: () => circ.destroy(),
    });

    circTimeline.play();
  }
}

export type AudibleSourceConfig = {
  source: {
    x: number;
    y: number;
  };
  position?: PMath.Vector2;
  duration?: number;
  amplitude?: number;
  onTick?: () => void;
  onComplete?: () => void;
};

export class AudibleSource extends GameObjects.Sprite {
  public source: {
    x: number;
    y: number;
  };
  public duration: number;
  public amplitude: number;
  public offset: Phaser.Math.Vector2;
  public onTick?: () => void;
  public onComplete?: () => void;
  public dropRate = 60;
  public readonly isAudibleSource = true;

  public declare body: Physics.Arcade.Body;

  private dropRateCount = 0;
  private progress: number;

  constructor(
    {
      onTick,
      onComplete,
      source,
      duration = 10000,
      amplitude = 16,
      position: offset = new Phaser.Math.Vector2(),
    }: AudibleSourceConfig,
    ...args: ConstructorParameters<typeof GameObjects.Sprite>
  ) {
    super(...args);
    this.source = source;
    this.offset = offset;
    this.duration = duration;
    this.onTick = onTick;
    this.onComplete = onComplete;
    this.amplitude = amplitude;

    this.progress = 0;

    this.setVisible(false);
  }

  public update() {
    if (this.progress >= this.duration) {
      this.destroy();
    } else {
      this.progress += 1;
      this.dropRateCount += 1;
    }
  }

  public dropMarker() {
    // Rate limit visual indicators.
    if (this.dropRateCount <= this.dropRate) {
      return;
    } else {
      this.dropRateCount = 0;
    }
    const parentBody = this.parentContainer?.body as Physics.Arcade.Body;
    const x = (parentBody?.x || 0) + this.x;
    const y = (parentBody?.y || 0) + this.y;

    const marker = this.scene.add.sprite(x, y, this.texture);
    marker.setAlpha(0.5);
    marker.setTint(0xcccccc);
    marker.setDepth(DEPTHS.audibles);

    this.scene.tweens.add({
      targets: marker,
      props: {
        alpha: 0,
      },
      duration: 500,
      onComplete: () => marker.destroy(),
    });

    createAudibleAnimation({
      x: x,
      y: y,
      width: 32,
      height: 32,
      color: 0xff0000,
      scene: this.scene,
    });
  }

  public isAudible() {
    return this.progress < this.duration;
  }
}

type AudibleListenerConfig = {
  owner: Entity;
  sensitivity?: number;
};

export class AudibleListener extends GameObjects.Sprite {
  public owner: Entity;
  public sensitivity: number;
  public declare body: Physics.Arcade.Body;
  public readonly isAudibleListener = true;

  constructor(
    { owner, sensitivity = 100 }: AudibleListenerConfig,
    ...args: ConstructorParameters<typeof GameObjects.Sprite>
  ) {
    super(...args);
    this.owner = owner;
    this.sensitivity = sensitivity;
    this.setAlpha(0);
  }
}

export class AudibleManager {
  public scene: Scene;
  private audibleSources: Physics.Arcade.Group;
  private audibleListeners: Physics.Arcade.Group;

  public collider: Physics.Arcade.Collider;

  constructor(scene: Phaser.Scene) {
    this.scene = scene;
    this.audibleSources = this.scene.physics.add.group();
    this.audibleSources.defaults = {} as unknown as any;
    this.audibleListeners = this.scene.physics.add.group();
    this.audibleListeners.defaults = {} as unknown as any;

    this.audibleSources.runChildUpdate = true;
    this.audibleListeners.runChildUpdate = true;

    const collider = this.scene.physics.add.collider(
      this.audibleSources,
      this.audibleListeners
    );
    collider.overlapOnly = true;

    this.collider = collider;

    scene.physics.world.on(
      Phaser.Physics.Arcade.Events.OVERLAP,
      function (obj1: GameObjects.GameObject, obj2: GameObjects.GameObject) {
        const source = obj1 as AudibleSource;
        const listener = obj2 as AudibleListener;

        if (source.isAudible()) {
          listener.emit('heard', source);
        }
      }
    );
  }

  public addSource(source: AudibleSource): void {
    this.audibleSources.add(source, true);
    source.body.onOverlap = true;
    source.body.setCircle(source.amplitude);
    source.body.setOffset(
      -source.amplitude + source.width,
      -source.amplitude + source.height
    );
    source.body.debugBodyColor = 0x00ff00;
  }

  public addListener(listener: AudibleListener): void {
    this.audibleListeners.add(listener, true);
    listener.body.onOverlap = true;
    listener.body.setCircle(listener.sensitivity);
    listener.body.setOffset(
      -listener.sensitivity + listener.width / 2,
      -listener.sensitivity + listener.height / 2
    );
    listener.body.debugBodyColor = 0xff0000;
  }
}
