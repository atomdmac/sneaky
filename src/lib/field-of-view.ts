import { Entity } from '@src/entities/entity';
import SneakyScene from '@src/scenes/i-sneaky-scene';
import { Mrpas } from 'mrpas';
import { Tilemaps } from 'phaser';

export class FieldOfView {
  private scene: SneakyScene;
  private parent: Entity;
  private fov: Mrpas;
  private fovDistance = 10;
  private current: Map<Tilemaps.Tile, Tilemaps.Tile>;

  constructor({ scene, parent }: { scene: SneakyScene; parent: Entity }) {
    this.scene = scene;
    this.parent = parent;
    const isTransparent = (x: number, y: number) => {
      const tile = this.scene.map.getTileAt(x, y);
      return tile && !tile.collides;
    };
    this.fov = new Mrpas(
      this.scene.map.width,
      this.scene.map.height,
      isTransparent
    );

    this.current = new Map();

    this.fovMarkAsVisible = this.fovMarkAsVisible.bind(this);
    this.fovWasMarkedVisible = this.fovWasMarkedVisible.bind(this);
  }

  public update(): void {
    this.current.clear();

    const camera = this.scene.cameras.main;

    this.scene.map
      .getLayer('fieldOfView')
      .tilemapLayer.getTilesWithinShape(camera.getBounds())
      .forEach((t: any) => t.setAlpha(1));
    this.fov.compute(
      this.scene.map.worldToTileX(this.parent.x + this.scene.map.tileWidth / 2),
      this.scene.map.worldToTileY(
        this.parent.y + this.scene.map.tileHeight / 2
      ),
      this.fovDistance,
      this.fovWasMarkedVisible,
      this.fovMarkAsVisible
    );
  }

  public canSee(x: number, y: number) {
    const tile = this.scene.map.getTileAt(x, y);
    return tile && this.current.has(tile);
  }

  private fovWasMarkedVisible(x: number, y: number) {
    const tile = this.scene.map.getTileAt(x, y);
    return tile && tile.alpha > 0.25;
  }

  private fovMarkAsVisible(x: number, y: number) {
    const fovLayer = this.scene.map.getLayer('fieldOfView');
    const terrainLayer = this.scene.map.getLayer('floor');

    const terrainTile = terrainLayer.tilemapLayer.getTileAt(x, y);
    const fovTile = fovLayer.tilemapLayer.getTileAt(x, y);

    if (terrainTile) {
      this.current.set(terrainTile, terrainTile);
      const distance = Phaser.Math.Distance.Between(
        x,
        y,
        this.scene.map.worldToTileX(this.parent.x),
        this.scene.map.worldToTileY(this.parent.y)
      );
      const alpha = (distance - this.fovDistance * 0.1) / this.fovDistance;
      if (fovTile) fovTile.setAlpha(alpha);
    }
  }
}
