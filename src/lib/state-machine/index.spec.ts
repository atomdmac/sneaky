import { State } from '.';

describe('State', function () {
  it('should be created', () => {
    expect.assertions(6);
    const config = {
      id: 'test-state',
      context: {},
    };
    const state = new State(config);
    expect(state.id).toBe(config.id);
    expect(state.context).toBe(config.context);
    expect(state.transitions).toBeInstanceOf(Map);
    expect(state.children).toBeInstanceOf(Map);
    expect(state.parent).toBeNull();
    expect(state.getCurrentStatePath()).toBe('test-state');
  });

  it.todo('should set a initial state');

  it('should set a default initial state if not provided', () => {
    const root = new State({
      id: 'root',
      children: [
        {
          id: 'child1',
          children: [
            {
              id: 'child1a',
              children: [
                {
                  id: 'child1b',
                },
              ],
            },
          ],
        },
      ],
    });

    expect(root.getCurrentStatePath()).toBe('root.child1.child1a.child1b');
  });

  describe('getChildState', () => {
    let root: State;

    beforeEach(() => {
      root = new State({
        id: 'root',
        children: [{ id: 'child1', children: [{ id: 'child2' }] }],
      });
    });

    it('should return a reference to the specified child state', () => {
      expect.assertions(1);
      expect(root.getChildState('child1.child2')).toBeInstanceOf(State);
    });

    it('should throw error if path to child state is invalid', () => {
      expect.assertions(1);
      const testFn = () => root.getChildState('child1.child2.notReal');
      expect(testFn).toThrowError(
        'Unable to find child state "notReal" in parent state "child2"'
      );
    });
  });

  describe('trigger', () => {
    let root: State;

    beforeEach(() => {
      root = new State({
        id: 'root',
        transitions: [
          {
            triggerId: 'goToChild2',
            toStateId: 'child2',
          },
          {
            triggerId: 'goToChild1',
            toStateId: 'child1',
          },
        ],
        children: [
          {
            id: 'child1',
            transitions: [
              {
                triggerId: 'goToChild1b',
                toStateId: 'child1b',
              },
            ],
            children: [
              {
                id: 'child1a',
              },
              {
                id: 'child1b',
              },
            ],
          },
          {
            id: 'child2',
            children: [
              {
                id: 'child2a',
              },
              {
                id: 'child2b',
              },
            ],
          },
        ],
      });
    });

    it('should transition between states', () => {
      expect.assertions(4);

      root.events.onEnter.on((...rest) => console.log('ENTER', rest[1].id));
      root.events.onExit.on((...rest) => console.log('EXIT', rest[1].id));

      expect(root.getCurrentStatePath()).toBe('root.child1.child1a');
      root.trigger('goToChild1b');
      expect(root.getCurrentStatePath()).toBe('root.child1.child1b');
      root.trigger('goToChild2');
      expect(root.getCurrentStatePath()).toBe('root.child2.child2a');
      root.trigger('goToChild1');
      expect(root.getCurrentStatePath()).toBe('root.child1.child1b');
    });

    it('should throw an error if attempting to change to invalid state', () => {
      expect.assertions(1);

      const testFn = () => root.changeState('not-a-real-state');

      expect(testFn).toThrowError(
        'Sub-state not-a-real-state does not exist in state root'
      );
    });

    it('should forward unhandled transitions to the root state', () => {
      expect.assertions(4);

      const root = new State({
        id: 'root',
        children: [
          {
            id: 'child1',
            children: [
              {
                id: 'child1a',
                children: [
                  {
                    id: 'child1b',
                  },
                ],
              },
            ],
          },
          {
            id: 'child2',
          },
        ],
      });
      root.addTransition({
        triggerId: 'goToChild2',
        toStateId: 'child2',
      });

      const currentStatePath = root.getCurrentStatePath();
      expect(currentStatePath).toBe('root.child1.child1a.child1b');
      const child1b = root.getChildState(currentStatePath);
      expect(child1b).toBeInstanceOf(State);
      expect(child1b.id).toBe('child1b');

      child1b.trigger('goToChild2');

      expect(root.getCurrentStatePath()).toBe('root.child2');
    });

    it('should handle "from state" restrictions', () => {
      const root: State = new State({
        id: 'root',
        children: [
          {
            id: 'child1',
          },
          {
            id: 'child2',
          },
          {
            id: 'child3',
          },
        ],
        transitions: [
          {
            triggerId: 'next',
            fromStateId: 'child1',
            toStateId: 'child2',
          },
          {
            triggerId: 'next',
            fromStateId: 'child2',
            toStateId: 'child3',
          },
          {
            triggerId: 'next',
            fromStateId: 'child3',
            toStateId: 'child1',
          },
        ],
      });

      expect(root.getCurrentStatePath()).toBe('root.child1');
      root.trigger('next');
      expect(root.getCurrentStatePath()).toBe('root.child2');
      root.trigger('next');
      expect(root.getCurrentStatePath()).toBe('root.child3');
      root.trigger('next');
      expect(root.getCurrentStatePath()).toBe('root.child1');
    });
  });

  describe('getCurrentStatePath', () => {
    it('should return an array of all nested current states', () => {
      expect.assertions(1);

      const root = new State({
        id: 'root',
        children: [
          {
            id: 'child1',
            children: [
              {
                id: 'child1a',
              },
            ],
          },
        ],
      });

      expect(root.getCurrentStatePath()).toBe('root.child1.child1a');
    });
  });

  describe('Transition Callbacks', () => {
    let root: State;
    const child1Enter = jest.fn();
    const child1Exit = jest.fn();
    const child2Enter = jest.fn();

    beforeEach(() => {
      child1Enter.mockReset();
      child1Exit.mockReset();
      child2Enter.mockReset();

      root = new State({
        id: 'root',
        children: [
          {
            id: 'child1',
            onExit: child1Exit,
            onEnter: child1Enter,
            children: [
              {
                id: 'child1a',
                onExit: child1Exit,
                onEnter: child1Enter,
              },
            ],
          },
          {
            id: 'child2',
            onEnter: child2Enter,
            children: [
              {
                id: 'child2a',
                onEnter: child2Enter,
              },
            ],
          },
        ],
      });
    });

    it('should fire callbacks for enter/exit', () => {
      expect.assertions(5);

      root.addTransition({ triggerId: 'goToChild2', toStateId: 'child2' });

      expect(child1Enter).toHaveBeenCalledTimes(2);
      expect(child1Exit).toHaveBeenCalledTimes(0);
      expect(child2Enter).toHaveBeenCalledTimes(0);

      const currentState = root.getChildState();
      currentState.trigger('goToChild2');

      expect(child1Exit).toHaveBeenCalledTimes(2);
      expect(child2Enter).toHaveBeenCalledTimes(2);
    });
  });

  describe('isFinal', () => {
    it('should return true if state has no children and no transitions', () => {
      const state = new State({ id: 'test-state' });
      expect(state.isFinal()).toBe(true);
    });

    it('should return false if state has children and transitions', () => {
      const state = new State({
        id: 'test-state',
        children: [
          {
            id: 'child1',
          },
        ],
      });
      expect(state.isFinal()).toBe(false);
    });
  });
});
