// TODO: Add (de)serialize methods
// TODO: Provide a place to listen for all events within a state tree
// TODO: Provide option to reset "initial child state" when re-entering a parent state

import PubSub from '../pub-sub';

export type Transition = {
  triggerId: string;
  toStateId: string;
  fromStateId?: string;
};

export type TransitionCallbackFn = (
  transition: Transition,
  newState: State
) => void;

export type OnUpdateCallback = (currentState: State) => void;

export type StateOptions = {
  id: string;
  context?: unknown;
  parent?: State | null;
  children?: StateOptions[];
  transitions?: Transition[];
  initialChildId?: string;
  onEnter?: TransitionCallbackFn;
  onExit?: TransitionCallbackFn;
  onUpdate?: OnUpdateCallback;
};

// TODO: Make context types more specific than `unknown`
export class State {
  public readonly id: string;
  public context?: unknown;
  public transitions: Map<string, Transition[]> = new Map();
  public parent: State | null = null;
  public children: Map<string, State> = new Map();
  public events: {
    onEnter: PubSub<TransitionCallbackFn>;
    onExit: PubSub<TransitionCallbackFn>;
  };
  private _currentChild?: State;
  private _onExitCallback?: TransitionCallbackFn;
  private _onEnterCallback?: TransitionCallbackFn;
  private _onUpdateCallback?: OnUpdateCallback;

  constructor({
    id,
    context,
    onEnter,
    onExit,
    onUpdate,
    parent = null,
    children = [],
    transitions = [],
    initialChildId = undefined,
  }: StateOptions) {
    this.id = id;
    this.context = context;
    this.parent = parent;
    this._onEnterCallback = onEnter && onEnter.bind(this);
    this._onExitCallback = onExit && onExit.bind(this);
    this._onUpdateCallback = onUpdate && onUpdate.bind(this);
    this.events = {
      onEnter: new PubSub(),
      onExit: new PubSub(),
    };

    this.transitions = new Map();
    if (transitions) {
      transitions.forEach((transition) => this.addTransition(transition));
    }

    // Add any child states included in the configuration.
    this.children = new Map();
    if (children) {
      children.forEach((child) =>
        this.addChildState(new State({ ...child, parent: this }))
      );
    }

    // If any child states exist, determine which should be the initial state.
    if (initialChildId) {
      this.changeState(initialChildId);
    } else if (this.children.size > 0) {
      const firstChild = this.children.values().next().value as State;
      this.changeState(firstChild.id);
    }

    // Fire the initial "onEnter" event handlers if this is the root node.
    if (!this.parent) {
      this.handleEnter(
        {
          triggerId: 'initial',
          toStateId: this.id,
          fromStateId: 'initial',
        },
        this._currentChild as State
      );
    }
  }

  /**
   * Return a dot-delimited string indicating the path to the most deeply nested
   * child state.  The first element of the path will be this object's ID.
   */
  public getCurrentStatePath(): string {
    if (this._currentChild) {
      return `${this.id}.${this._currentChild.getCurrentStatePath()}`;
    } else {
      return this.id;
    }
  }

  /**
   * Given a dot-delimited string or an array, attempt to get a reference to the
   * specified nested state.  If no path is given, return the currently active
   * nexted state.  Throws an error if the state does not exist.
   */
  public getChildState(path?: string | string[]) {
    let current: State = this as State;
    if (!path) {
      path = this.getCurrentStatePath();
    }
    if (typeof path === 'string') {
      path = path.split('.');
    }

    for (let i = 0; i < path.length; i++) {
      // If top-level state's ID was included in the path, ignore it.
      if (i === 0 && path[i] === this.id) {
        continue;
      }
      const next = current.children.get(path[i]);
      if (next) {
        current = next;
      } else {
        throw new Error(
          `Unable to find child state "${path[i]}" in parent state "${current.id}"`
        );
      }
    }

    return current;
  }

  /**
   * Add a direct child state to this state.
   */
  public addChildState(state: State) {
    this.children.set(state.id, state);
    if (this.children.size === 1) {
      this._currentChild = state;
    }
    state.events.onEnter.on(
      this.events.onEnter.notify.bind(this.events.onEnter)
    );
  }

  public addTransition(transition: Transition) {
    let transitionList = this.transitions.get(transition.triggerId);
    if (!transitionList) {
      transitionList = [];
      this.transitions.set(transition.triggerId, transitionList);
    }
    transitionList.push(transition);
  }

  public getTransition(
    triggerId: string,
    fromStateId?: string
  ): Transition | undefined {
    const transitionList = this.transitions.get(triggerId);
    if (!transitionList) {
      return undefined;
    }
    return transitionList.find(
      (t) =>
        t.triggerId === triggerId &&
        (!t.fromStateId || t.fromStateId === fromStateId)
    );
  }

  public changeState(stateId: string): State {
    const newState = this.children.get(stateId);
    if (newState) {
      this._currentChild = newState;
      return newState;
    } else {
      throw new Error(
        `Sub-state ${stateId} does not exist in state ${this.id}`
      );
    }
  }

  public handleUpdate() {
    if (this._onUpdateCallback) {
      this._onUpdateCallback(this);
    }
    if (this._currentChild) {
      this._currentChild.handleUpdate();
    }
  }

  public handleEnter(transition: Transition, state: State) {
    if (this._onEnterCallback) {
      this._onEnterCallback(transition, state);
    }
    if (this._currentChild) {
      this._currentChild.handleEnter(transition, state);
    }
    if (this.children.size === 0) {
      this.events.onEnter.notify(transition, this);
    }
  }

  public handleExit(transition: Transition, state: State) {
    if (this._onExitCallback) {
      this._onExitCallback(transition, state);
    }
    if (this._currentChild) {
      this._currentChild.handleExit(transition, state);
    }
    if (this.children.size === 0) {
      this.events.onExit.notify(transition, this);
    }
  }

  /**
   * Attempt to transition to a new state based on the given trigger.  If the
   * current state is unable to handle the trigger, the request will be forwarded
   * up the hierarchy until a suitable state is found.  If no such state is found,
   * an error is thrown.
   */
  public trigger(triggerId: string, currentState?: State) {
    currentState = currentState || this.getChildState() || this;

    const transition: Transition | undefined = currentState.getTransition(
      triggerId,
      this.getChildState().id
    );

    if (transition) {
      // If the current state can handle the transition, handle it now.
      currentState.handleExit(transition, currentState);
      const newState = currentState.changeState(transition.toStateId);
      newState.handleEnter(transition, newState);
    } else if (currentState.parent) {
      // If current state CANNOT be handled here, forward it up the chain.
      currentState.parent.trigger(triggerId, currentState.parent);
    } else {
      throw new Error(`Unhandled trigger "${triggerId}"`);
    }
  }

  public isFinal() {
    return this.transitions.size === 0 && this.children.size === 0;
  }
}
