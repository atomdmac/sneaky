import { Cell, Floor } from '../floor';
import * as ROT from 'rot-js';

export class AStar {
  floor: Floor;

  constructor (floor: Floor) {
    this.floor = floor;

    // Scope functions to this object.
    this.passableCallback = this.passableCallback.bind(this);
  }

  getPathBetween (x1: number, y1: number, x2: number, y2: number) {
    const aStar = new ROT.Path.AStar(x1, y1, this.passableCallback);
    const path: Cell[] = [];
    aStar.compute(x2, y2, (x, y) => {
      path.push(
        this.floor.getCell(x, y)
      )
    });
    return path;
  }

  passableCallback (x: number, y: number) {
    const cell: Cell = this.floor.getCell(x, y);
    if (cell && cell.passable) {
      return true;
    } else {
      return false;
    }
  }
}
