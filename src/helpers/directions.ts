import { DIRS } from 'rot-js';

export function getDirectionBetween(x1: number, y1: number, x2: number, y2: number): number {
  const x: number = Math.sign(x2 - x1);
  const y: number = Math.sign(y2 - y1);
  let dirIndex: number;
  DIRS[8].forEach((d, idx) => {
    if (d[0] === x && d[1] === y) {
      dirIndex = idx;
    }
  });
  return dirIndex;
}

export function areAdjacent (x1: number, y1: number, x2: number, y2: number): boolean {
  return Math.abs(x1 - x2) <= 1 && Math.abs(y1 - y2) <= 1;
}
