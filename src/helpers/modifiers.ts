import { IModdable, IModifier } from '../components';

export function hasModifier(component: IModdable, id: string): boolean {
  return !!component.modifiers.find((mod: IModifier) => mod.id === id);
}

export function addModifier (component: IModdable, modifier: IModifier) {
  if (hasModifier(component, modifier.id)) {
    throw new Error('Cannot apply the same modifier twice');
  }
  component.modifiers.push(modifier);
}

export function removeModifier (component: IModdable, id: string) {
  for (let i = 0; i < component.modifiers.length; i++) {
    if (component.modifiers[i].id === id) {
      component.modifiers.splice(i, 1);
      break;
    }
  }
}
