export interface Score {
  date: number;
  floor: number;
  loot: number;
}

export function isHighScore (newScore: Score): boolean {
  const score = getScore();
  if (!score) return true;
  const newTotal = newScore.loot + newScore.floor;
  const oldTotal = score.loot + score.floor;

  console.log(newTotal, oldTotal);

  return newTotal > oldTotal;
}

export function getScore (): Score {
  const raw = localStorage.getItem('high-scores');
  console.log('raw: ', raw);
  return JSON.parse(raw) as Score;
}

export function setScore (newScore: Score) {
  const json = JSON.stringify(newScore);
  console.log('json: ', json);
  localStorage.setItem('high-scores', json);
}
