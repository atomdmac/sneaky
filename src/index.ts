import * as ROT from 'rot-js';
import {
  LoseState,
  PlayState,
  SplashState,
} from './states';
import { Game } from './game';
import { Renderer } from './renderer';
import GameSettings from './settings';

const bootstrap = () => {
  const main = new ROT.Display({
    width: GameSettings.width,
    height: GameSettings.height + GameSettings.hudPadding,
    fontSize: GameSettings.fontSize,
    forceSquareRatio: GameSettings.forceSquareRatio
  });
  const effects = new ROT.Display({
    width: GameSettings.width,
    height: GameSettings.height + GameSettings.hudPadding,
    fontSize: GameSettings.fontSize,
    forceSquareRatio: GameSettings.forceSquareRatio,
    bg: 'rgba(0,0,0,0)'
  });

  // TODO: Move display layer ordering to renderer.
  const renderer = new Renderer();
  renderer.addDisplay('main', main);
  renderer.addDisplay('effects', effects);

  const game = new Game();
  game.addState('splash', new SplashState(renderer, game));
  game.addState('play', new PlayState(renderer, game));
  game.addState('lose', new LoseState(renderer, game));

  game.setState('splash');

  game.unpause();
}

bootstrap();
