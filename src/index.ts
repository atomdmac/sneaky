import Phaser from 'phaser';
import config from '@src/config';
import './index.css';

new Phaser.Game(config);
