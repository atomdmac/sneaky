import { GameObjects } from 'phaser';

export type ProgressBarConfig = {
  width?: number;
  height?: number;
  x?: number;
  y?: number;
  scene: Phaser.Scene;
};

export default class ProgressBar extends GameObjects.Container {
  private _current: number;
  private _total: number;

  private graphics: GameObjects.Graphics;

  constructor({ width = 50, height = 5, x, y, scene }: ProgressBarConfig) {
    const graphics = scene.add.graphics();
    super(scene, x, y, [graphics]);
    this.graphics = graphics;

    this.width = width;
    this.height = height;

    this._current = 0;
    this._total = 100;

    this.draw();
  }

  draw() {
    this.graphics.clear();

    // Background
    this.graphics.fillStyle(0x000000, 1);
    this.graphics.fillRect(0, 0, this.width, this.height);

    // Progress bar
    this.graphics.fillStyle(0xff0000, 1);
    this.graphics.fillRect(
      0,
      0,
      (this._current * this.width) / this._total,
      this.height
    );

    // Outline
    this.graphics.lineStyle(1, 0xffffff, 1);
    this.graphics.strokeRect(0, 0, this.width, this.height);
  }

  setTotal(newTotal: number) {
    this._total = newTotal;
    this.draw();
  }

  setCurrent(newCurrent: number) {
    this._current = newCurrent;
    this.draw();
  }

  setWidth(newWidth: number) {
    this.width = newWidth;
    this.draw();
  }

  setHeight(newHeight: number) {
    this.height = newHeight;
    this.draw();
  }
}
