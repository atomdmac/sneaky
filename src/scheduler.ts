import Scheduler from 'rot-js/lib/scheduler/scheduler';
import Speed from 'rot-js/lib/scheduler/speed';
import {SpeedComponent} from './components';
import {Entity} from './entities';

export class Schedulable {
  private entity: Entity;

  constructor(entity: Entity) {
    this.entity = entity;
  }

  getEntity () {
    return this.entity;
  }

  getSpeed (): number {
    if (this.entity.hasComponent(SpeedComponent)) {
      return this.entity.getComponentValue(SpeedComponent, 'base');
    }
    else {
      return 0;
    }
  }
}

export class InitiativeTracker {
  public scheduler: Scheduler;

  constructor () {
    this.scheduler = new Speed();
  }

  add (entity: Entity, repeat: boolean = false) {
    this.scheduler.add(
      new Schedulable(entity), 
      repeat
    );
  }

  next () {
    this.scheduler.next();
  }

  remove () {
    throw new Error('You forgot to implement the remove() method.');
  }

  clear () {
    this.scheduler.clear();
  }

  getTime () {
    this.scheduler.getTime();
  }

  getTimeOf () {
    throw new Error('You forgot to implement the getTimeOf() method.');
  }

  getCurrent (): Entity {
    return (this.scheduler._current as Schedulable).getEntity();
  }
}

