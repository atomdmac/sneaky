import {DoorComponent, PositionComponent, VisibleComponent} from '../components';
import { Entity } from './entity';

export class Door extends Entity {
  constructor () {
    super();
    const vis = new VisibleComponent(this);
    vis.character = '>';
    vis.backgroundColor = '#000';
    vis.foregroundColor = '#fff';

    this.addComponent(new DoorComponent(this));
    this.addComponent(new PositionComponent(this));
    this.addComponent(vis);
  }
}

