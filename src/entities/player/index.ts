import Phaser from 'phaser';
import { Entity } from '@src/entities/entity';
import { AudibleListener, AudibleSource } from '@src/lib/audible';
import { State, Transition } from '@src/lib/state-machine';
import { hasLineOfSight } from '@src/maps/collision';
import SneakyScene from '@src/scenes/i-sneaky-scene';
import StatusIndicator from '../status-indicator';
import createStateMachine, { States, Triggers } from './state-machine';
import { FieldOfView } from '@src/lib/field-of-view';
import { getRandomSpawnPoints } from '@src/maps/spawn';

export class Player extends Entity {
  public declare stateMachine: State;
  public declare dashBlur: Phaser.GameObjects.Particles.ParticleEmitter;
  public fov: FieldOfView;
  private declare statuses: {
    wait: StatusIndicator;
    slow: StatusIndicator;
  };

  static EntityType = 'player';

  constructor(...args: ConstructorParameters<typeof Entity>) {
    super(...args);

    this._entityType = Player.EntityType;

    this.handleStateEnter = this.handleStateEnter.bind(this);
    this.handleHeard = this.handleHeard.bind(this);

    const sprite = this.scene.add.sprite(16, 16, 'player');
    this.add(sprite);

    this.fov = new FieldOfView({
      scene: this.scene,
      parent: this,
    });

    this.body.setSize(28, 28);
    this.body.offset.x = 2;
    this.body.offset.y = 2;
    this.setUpStateMachine();
    this.setUpParticles();
    this.setUpStatusIndicators();
    this.setUpAudibles();
  }

  private setUpStateMachine() {
    this.stateMachine = createStateMachine(this);
    this.stateMachine.events.onEnter.on(this.handleStateEnter);
  }

  private handleStateEnter(transition: Transition) {
    if (transition.toStateId === States.SLOW_WALK) {
      this.statuses.slow.show(0);
    } else if (transition.toStateId === States.WALK) {
      this.statuses.slow.hide();
    }
  }

  private setUpParticles() {
    const mgr = this.scene.add.particles('player');
    this.dashBlur = mgr.createEmitter({
      maxParticles: 10,
      lifespan: 75,
      alpha: 0.2,
      follow: this,
      followOffset: {
        x: 16,
        y: 16,
      },
    });
    this.dashBlur.stop();
  }

  private setUpStatusIndicators() {
    // Set up status indicators.
    this.statuses = {
      slow: new StatusIndicator(this.scene, 16, 0, 'status-slow'),
      wait: new StatusIndicator(this.scene, 16, 0, 'status-wait'),
    };
    this.add(this.statuses.slow);
    this.add(this.statuses.wait);
  }

  private setUpAudibles() {
    const audibleListener = new AudibleListener(
      {
        owner: this,
        sensitivity: 100,
      },
      this.scene,
      16,
      16,
      'status-slow'
    );

    audibleListener.on('heard', this.handleHeard);

    this.scene.audible.addListener(audibleListener);
    this.add(audibleListener);
  }

  private handleHeard(source: AudibleSource) {
    if (
      !hasLineOfSight(
        this.body.position,
        new Phaser.Math.Vector2(source.source),
        this.scene.map,
        []
      )
    ) {
      source.dropMarker();
    }
  }

  public update(time: number, delta: number): void {
    super.update(time, delta);
    this.inventory.update(time, delta);
    this.stateMachine.handleUpdate();
    this.fov.update();
  }

  public sneak() {
    const currentStateName = this.stateMachine.getChildState().id;
    if (currentStateName.indexOf(States.WALK) === -1) {
      return false;
    }

    this.stateMachine.trigger(Triggers.SLOW_DOWN);
    return true;
  }

  public dash() {
    const currentStateName = this.stateMachine.getChildState().id;
    if (currentStateName.indexOf(States.WALK) === -1) {
      return false;
    }

    this.stateMachine.trigger(Triggers.SPEED_UP);
    return true;
  }

  public move(x: number, y: number): boolean {
    this.statuses.wait.hide();

    if (this.stateMachine.getCurrentStatePath().indexOf(States.DASH) > -1) {
      return false;
    }

    const { baseSpeed } = this.locomotion;

    this.body.velocity.x = x * baseSpeed;
    this.body.velocity.y = y * baseSpeed;

    return true;
  }

  public wait() {
    this.statuses.wait.show(0);
  }
}

export type PlayerConfig = {
  x?: number;
  y?: number;
  scene: SneakyScene;
  maxSpeed: number;
  dragMultiplier: number;
};

export function createPlayer(playerConfig: PlayerConfig) {
  const { x = 0, y = 0, scene, maxSpeed, dragMultiplier } = playerConfig;

  const player = new Player(scene, x, y, [], maxSpeed, dragMultiplier);
  player.setActive(true);

  scene.map.scene.add.existing(player);

  // const spawnsLayer = scene.map.getObjectLayer('spawns');
  // if (!spawnsLayer) {
  //   throw new Error('Layer "spawns" not found in map.');
  // }
  // const playerSpawn = spawnsLayer.objects.find(
  //   (o) => o.name === 'Player_Spawn'
  // );
  const playerSpawn = getRandomSpawnPoints(scene.map, 1).map(({ x, y }) => ({
    x: x * 32,
    y: y * 32,
  }))[0];

  if (playerSpawn?.x && playerSpawn?.y) {
    player.x = playerSpawn.x;
    player.y = playerSpawn.y;
  }

  return player;
}
