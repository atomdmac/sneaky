import { State, StateOptions } from '@src/lib/state-machine';
import { Player } from '.';

export type PlayerContext = {
  dash: {
    cooldownTicks: number;
    maxTicks: number;
    tickCount: number;
    speedMultiplier: number;
  };
};

export const Triggers = {
  SPEED_UP: 'speed_up',
  SLOW_DOWN: 'slow_down',
};

export const States = {
  SLOW_WALK: 'slow_walk',
  WALK: 'normal_walk',
  DASH: 'dash',
};

export default function create(player: Player) {
  const context: PlayerContext = {
    dash: {
      maxTicks: 15,
      cooldownTicks: 60,
      tickCount: 0,
      speedMultiplier: 3,
    },
  };

  const slowWalk: StateOptions = {
    id: States.SLOW_WALK,
    onEnter: () => {
      const { baseSpeed } = player.locomotion;
      player.body.setMaxVelocity(baseSpeed / 2, baseSpeed / 2);
    },

    onUpdate: () => {
      const {
        dash: { cooldownTicks, tickCount },
      } = context;
      if (tickCount >= cooldownTicks) {
        context.dash.tickCount = 0;
        stateMachine.trigger(Triggers.SPEED_UP);
      } else {
        context.dash.tickCount++;
      }
    },
  };

  const walk: StateOptions = {
    id: States.WALK,
    onEnter: () => {
      const { baseSpeed } = player.locomotion;
      player.body.maxVelocity.setLength(baseSpeed);
    },
  };

  const dash: StateOptions = {
    id: States.DASH,
    onEnter: () => {
      const {
        dash: { speedMultiplier },
      } = context;
      player.body.maxVelocity.setLength(
        player.locomotion.baseSpeed * speedMultiplier
      );
    },
    onUpdate: () => {
      const {
        dash: { maxTicks, tickCount, speedMultiplier },
      } = context;

      const { baseSpeed } = player.locomotion;
      player.body.velocity.setLength(baseSpeed * speedMultiplier);

      if (tickCount >= maxTicks) {
        context.dash.tickCount = 0;
        stateMachine.trigger(Triggers.SLOW_DOWN);
      } else {
        context.dash.tickCount++;
      }
    },
  };

  const stateMachine = new State({
    id: 'player',
    children: [slowWalk, walk, dash],
    initialChildId: States.WALK,
    transitions: [
      {
        triggerId: Triggers.SPEED_UP,
        fromStateId: States.SLOW_WALK,
        toStateId: States.WALK,
      },
      {
        triggerId: Triggers.SPEED_UP,
        fromStateId: States.WALK,
        toStateId: States.DASH,
      },
      {
        triggerId: Triggers.SLOW_DOWN,
        fromStateId: States.DASH,
        toStateId: States.SLOW_WALK,
      },
      {
        triggerId: Triggers.SLOW_DOWN,
        fromStateId: States.WALK,
        toStateId: States.SLOW_WALK,
      },
    ],
  });

  return stateMachine;
}
