import Phaser from 'phaser';

export default class StatusIndicator extends Phaser.GameObjects.Sprite {
  private originalPosition: Phaser.Math.Vector2;
  private declare showTween: Phaser.Tweens.Tween;
  private declare hideTween: Phaser.Tweens.Tween;

  constructor(
    ...args: ConstructorParameters<typeof Phaser.GameObjects.Sprite>
  ) {
    super(...args);

    this.setAlpha(0);
    this.originalPosition = new Phaser.Math.Vector2();
    this.originalPosition.setFromObject(this);
  }

  public show(duration = 250) {
    if (this.showTween?.isPlaying() || !this.scene) return;

    this.showTween = this.scene.tweens.add({
      duration: 250,
      targets: this,
      props: {
        alpha: 1,
        y: this.originalPosition.y - 16,
      },
      ease: Phaser.Math.Easing.Bounce.Out,
      completeDelay: duration,
      onComplete: () => duration && this.hide(),
    });
  }

  public hide() {
    if (this.hideTween?.isPlaying() || !this.scene) return;

    this.hideTween = this.scene.tweens.add({
      duration: 250,
      targets: this,
      props: {
        alpha: 0,
        y: this.originalPosition.y - 32,
      },
      onComplete: () => {
        this.x = this.originalPosition.x;
        this.y = this.originalPosition.y;
      },
    });
  }
}
