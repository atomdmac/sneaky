import Phaser from 'phaser';
import { Mob } from '@src/entities/mobs';
import { State, StateOptions } from '@src/lib/state-machine';
import { getRandomSpawnPoints } from '@src/maps/spawn';
import createPatrol from './states/patrol';
import createPursue from './states/pursue';
import createSearch from './states/search';
import StateNames from './states/states';
import Triggers from './states/triggers';
import createWander from './states/wander';

export function create(mob: Mob) {
  const alert: StateOptions = {
    id: StateNames.ALERT,
    children: [
      createPursue(mob, {
        quary: mob.scene.player,
        arriveDistance: 100,
        destination: new Phaser.Math.Vector2(),
      }),
      createSearch(mob, {
        quary: mob.scene.player,
        searchTime: 0,
        maxSearchTime: 100,
      }),
    ],
    transitions: [
      {
        triggerId: Triggers.ARRIVE,
        toStateId: StateNames.SEARCH,
      },
      {
        triggerId: Triggers.SEE_QUARY,
        toStateId: StateNames.PURSUE,
      },
    ],
  };

  const patrolPoints = getRandomSpawnPoints(mob.scene.map, 4).map((spawn) => [
    spawn.x,
    spawn.y,
  ]);
  const relaxed: StateOptions = {
    id: StateNames.RELAXED,
    initialChildId: StateNames.PATROL,
    children: [
      createWander(mob, {
        quary: mob.scene.player,
      }),
      createPatrol(mob, {
        quary: mob.scene.player,
        path: [],
        points: patrolPoints,
      }),
    ],
    transitions: [],
  };

  const state = new State({
    id: 'mob-behavior',
    initialChildId: StateNames.RELAXED,
    children: [alert, relaxed],
    transitions: [
      {
        triggerId: Triggers.GET_BORED,
        toStateId: StateNames.RELAXED,
      },
      {
        triggerId: Triggers.SEE_QUARY,
        toStateId: StateNames.ALERT,
      },
    ],
  });

  return state;
}
