import Phaser, { Physics, Tweens } from 'phaser';
import ISneakyScene from '@src/scenes/i-sneaky-scene';
import SneakyScene from '@src/scenes/i-sneaky-scene';
import { create as createMobMachine } from './behavior';
import { State, Transition } from '@src/lib/state-machine/index';
import { Entity } from '../entity';
import { getRandomSpawnPoints } from '@src/maps/spawn';
import StatusIndicator from '@src/entities/status-indicator';
import { AudibleSource } from '@src/lib/audible';

export class Mob extends Entity {
  public declare stateMachine: State;

  private sprite: Phaser.GameObjects.Sprite;
  private declare losGraphic: Phaser.GameObjects.Graphics;

  constructor(
    scene: SneakyScene,
    x: number,
    y: number,
    children?: Phaser.GameObjects.GameObject[]
  ) {
    super(scene, x, y, children);

    // Settings
    this.name = 'mob';
    this._entityType = Mob.EntityType;

    // Graphics
    const sprite = this.scene.add.sprite(16, 16, 'mob');
    this.sprite = sprite;
    this.add(sprite);

    // Physics
    this.body.setSize(30, 30);
    this.body.offset.x = 1;
    this.body.offset.y = 1;

    this.createLosGraphic();
    this.setUpStateMachine();
    this.setUpStatusIndicators();
    this.setUpAudibles();
  }

  private setUpStateMachine() {
    this.stateMachine = createMobMachine(this);
  }

  private setUpStatusIndicators() {
    const alertStatus = new StatusIndicator(
      this.scene,
      0,
      -16,
      'exclaimation-point'
    );
    this.add(alertStatus);

    this.stateMachine.events.onEnter.on((t: Transition) => {
      if (t.toStateId === 'alert') {
        alertStatus.show();
      }
    });

    this.on('die', () => {
      alertStatus.destroy();
    });
  }

  private setUpAudibles() {
    const position = new Phaser.Math.Vector2(32, 32);
    const sound = new AudibleSource(
      { source: this.body, amplitude: 50, position },
      this.scene,
      0,
      0,
      'mob'
    );
    this.scene.audible.addSource(sound);
    this.add(sound);

    this.on('die', () => {
      sound.destroy();
    });
  }

  private createLosGraphic() {
    const losGraphic = this.scene.add.graphics();
    losGraphic.x = 16;
    losGraphic.y = 16;
    this.add(losGraphic);
    losGraphic.fillGradientStyle(
      0xffff00,
      0xffff00,
      0xffff00,
      0xffff00,
      0.6,
      0,
      0,
      0.6
    );

    const losLength = this.sight.distance;
    const yOffset = Math.tan(this.sight.fieldOfViewRadians / 2) * losLength;

    losGraphic.fillTriangle(0, 0, losLength, -yOffset, losLength, yOffset);
    losGraphic.setRotation(this.direction.bearing.angle());
    this.sendToBack(losGraphic);
    this.losGraphic = losGraphic;
  }

  public update(time: number, delta: number): void {
    super.update(time, delta);
    if (this.scene.physics.world.isPaused) {
      return;
    }

    this.stateMachine.handleUpdate();

    this.losGraphic.setRotation(this.direction.bearing.angle());

    this.updateVisibility();
  }

  private declare visibilityTween: Tweens.Tween;

  private updateVisibility() {
    if (!this.visibilityTween) {
      this.visibilityTween = this.scene.tweens.add({
        targets: [this.sprite, this.losGraphic],
        props: {
          alpha: 0,
        },
        duration: 250,
      });
    }

    let newEndValue = 0;
    if (
      this.scene.player.fov.canSee(
        this.scene.map.worldToTileX(this.x),
        this.scene.map.worldToTileY(this.y)
      )
    ) {
      newEndValue = 1;
    } else {
      newEndValue = 0;
    }

    const newEndIsSame = this.visibilityTween.data.some(
      (d) => d.end === newEndValue
    );
    if (!newEndIsSame) {
      this.visibilityTween.updateTo('alpha', newEndValue);
      if (
        !this.visibilityTween.isPlaying() &&
        !this.visibilityTween.isPaused()
      ) {
        this.visibilityTween.play();
      }
    }
  }
}

export function createMobs(scene: ISneakyScene) {
  // Additional stuff to collide with
  const mobs: Physics.Arcade.Group = scene.physics.add.group({
    collideWorldBounds: true,
    runChildUpdate: true,
    classType: Mob,
    maxVelocityX: 100,
    maxVelocityY: 100,
  });

  const mobCount = 10;
  const spawnPoints = getRandomSpawnPoints(scene.map, mobCount);
  for (let m = 0; m < mobCount; m++) {
    const { x, y } = spawnPoints[m];
    mobs.create(scene.map.tileToWorldX(x), scene.map.tileToWorldY(y)) as Mob;
  }

  return mobs;
}
