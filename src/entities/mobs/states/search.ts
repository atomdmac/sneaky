import { State, StateOptions } from '@src/lib/state-machine';
import { hasLineOfSight } from '@src/maps/collision';
import { Entity } from '@src/entities/entity';
import { isWithinFieldOfView } from '@src/maps/collision';
import StateNames from './states';
import Triggers from './triggers';

export type SearchContext = {
  maxSearchTime: number;
  searchTime: number;
  quary: Entity | null;
};

export default function create(entity: Entity, defaultContext?: SearchContext) {
  const context = defaultContext || {
    maxSearchTime: 100,
    searchTime: 0,
    quary: null,
  };

  const search: StateOptions = {
    id: StateNames.SEARCH,
    onEnter: function () {
      entity.body.velocity.setLength(0);
    },
    onUpdate: function (currentState: State) {
      const { quary, maxSearchTime, searchTime } = context;
      if (!quary) {
        currentState.trigger(Triggers.GET_BORED);
        return;
      }
      const canSeeTarget =
        hasLineOfSight(
          entity.body.position,
          quary.body.position,
          entity.scene.map
        ) &&
        isWithinFieldOfView(
          entity.body.position,
          entity.direction.bearing,
          entity.sight.fieldOfViewRadians,
          quary.body.position
        );

      if (canSeeTarget) {
        currentState.trigger(Triggers.SEE_QUARY);
      } else {
        entity.direction.bearing.setAngle(
          entity.direction.bearing.angle() + 0.04
        );
      }

      if (searchTime >= maxSearchTime) {
        currentState.trigger(Triggers.GET_BORED);
      } else {
        context.searchTime += 1;
      }
    },
    onExit: function () {
      context.searchTime = 0;
    },
  };
  return search;
}
