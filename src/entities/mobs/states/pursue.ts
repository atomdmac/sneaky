import { Entity } from '@src/entities/entity';
import { State, StateOptions } from '@src/lib/state-machine';
import { hasLineOfSight, isWithinFieldOfView } from '@src/maps/collision';
import StateNames from './states';
import Triggers from './triggers';

export type PursueContext = {
  arriveDistance: number;
  destination: Phaser.Math.Vector2;
  quary: Entity | null;
};

export default function (entity: Entity, defaultContext?: PursueContext) {
  const context = defaultContext || {
    arriveDistance: 16,
    destination: new Phaser.Math.Vector2(),
    quary: null,
  };

  const pursue: StateOptions = {
    id: StateNames.PURSUE,
    transitions: [],
    onEnter: function () {
      const { destination, quary } = context;
      if (quary) {
        destination.copy(quary);
      }
    },
    onUpdate: function (currentState: State) {
      const { arriveDistance, destination, quary } = context;
      // Only chase quary if they are alive.
      if (!quary?.isAlive) {
        currentState.trigger(Triggers.GET_BORED);
        return;
      }
      const isWithinRange =
        entity.body.position.distance(quary) < entity.sight.distance;
      const canSeeTarget =
        isWithinRange &&
        hasLineOfSight(
          entity.body.position,
          quary.body.position,
          entity.scene.map
        ) &&
        isWithinFieldOfView(
          entity.body.position,
          entity.direction.bearing,
          entity.sight.fieldOfViewRadians,
          quary.body.position
        );

      if (canSeeTarget) {
        destination.copy(quary);
      } else {
        // We've arrived at the last point at which we saw the target.  Give up.
        const distance = entity.body.position.distance(destination);
        if (distance < arriveDistance) {
          currentState.trigger(Triggers.ARRIVE);
          return;
        }
      }

      const newAngle = entity.locomotion.moveTowards(
        destination.x,
        destination.y
      );
      entity.direction.bearing.setAngle(newAngle).normalize();
    },
  };

  return pursue;
}
