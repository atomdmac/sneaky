import Phaser from 'phaser';
import { State, StateOptions } from '@src/lib/state-machine';
import StateNames from './states';
import { Entity } from '@src/entities/entity';
import { canSeeTarget } from '@src/maps/collision';
import Triggers from './triggers';
import States from './states';

export type PatrolContext = {
  points: number[][];
  path: number[][];
  quary: Entity | null;
};

export default function create(entity: Entity, defaultContext?: PatrolContext) {
  const {
    scene: { map },
  } = entity;
  const context: PatrolContext = defaultContext || {
    points: [],
    path: [],
    quary: null,
  };

  function nextPatrolPoint() {
    if (context.points.length > 0) {
      context.points.push(context.points.shift() as number[]);
    } else {
      throw new Error('Switched to patrol state without any patrol points!');
    }
  }

  function prunePath(path: number[][]): number[][] {
    if (path.length <= 2) {
      return path;
    }
    const prunedPath = [];
    for (let i = 0; i < path.length; i++) {
      if (i === 0 || i === path.length - 1) {
        prunedPath.push(path[i]);
        continue;
      }
      const prevDelta = [
        path[i][0] - path[i - 1][0],
        path[i][1] - path[i - 1][1],
      ];
      const nextDelta = [
        path[i][0] - path[i + 1][0],
        path[i][1] - path[i + 1][1],
      ];
      if (prevDelta[0] !== nextDelta[0] && prevDelta[1] !== nextDelta[1]) {
        prunedPath.push(path[i]);
      }
    }
    return prunedPath;
  }

  function updatePath() {
    const offset = {
      x: 0, // (map.tileWidth - entity.body.width) / 2,
      y: 0, // (map.tileHeight - entity.body.height) / 2,
    };

    // TODO: Create fallback for when a path to the given point can't be found.
    const current = {
      // Increase by half map width in order to ensure best guess for entity's
      // current tile.
      x: map.worldToTileX(entity.x + entity.scene.map.tileWidth / 2),
      y: map.worldToTileY(entity.y + entity.scene.map.tileHeight / 2),
    };
    const next = {
      x: context.points[0][0],
      y: context.points[0][1],
    };
    let path = entity.scene.astar.findPath(current, next);

    // TODO: This is mostly here for debugging.
    if (path.length === 0) {
      console.log('Cannot find path from: ', current, 'to', next);
      throw 'PATH NOT FOUND.';
    }

    // Prune path to the fewest number of segments.
    path = prunePath(path);

    // Convert tile coordinates to world coordinates.
    context.path = path.map(([x, y]) => [
      map.tileToWorldX(x) + offset.x,
      map.tileToWorldY(y) + offset.y,
    ]);
  }

  function getAbsRads(rads: number) {
    return rads < 0 ? rads + Math.PI * 2 : rads;
  }

  function getRotationDiff(currentRads: number, targetRads: number) {
    return Phaser.Math.Difference(currentRads, targetRads);
  }

  function getRotationDirection(currentRads: number, targetRads: number) {
    const diff = getAbsRads(targetRads) - getAbsRads(currentRads);
    let dir = diff < 0 ? -1 : 1;

    const absDiff = getRotationDiff(currentRads, targetRads);
    if (absDiff > Math.PI) {
      dir *= -1;
    }

    return dir;
  }

  const targetVector = new Phaser.Math.Vector2();
  const patrolTurn: StateOptions = {
    id: States.PATROL_TURN,
    transitions: [],
    onEnter() {
      entity.body.velocity.setLength(0);
    },
    onUpdate(newState: State) {
      if (context.path.length === 0) {
        nextPatrolPoint();
        updatePath();
      }

      const [targetTile] = context.path;

      // TODO: Get rid of intermediate object creation for vector.
      targetVector.setFromObject({ x: targetTile[0], y: targetTile[1] });
      targetVector.subtract(entity.body);

      const diff = getRotationDiff(
        getAbsRads(entity.direction.bearing.angle()),
        getAbsRads(targetVector.angle())
      );

      if (diff < 0.1) {
        newState.trigger(Triggers.FINISH_TURNING);
      }

      const rotationDirection = getRotationDirection(
        getAbsRads(entity.direction.bearing.angle()),
        getAbsRads(targetVector.angle())
      );

      entity.direction.bearing.rotate(0.1 * rotationDirection);

      const {
        scene: { player },
      } = entity;

      if (player.isAlive && canSeeTarget(entity, player)) {
        newState.trigger(Triggers.SEE_QUARY);
      }
    },
  };

  nextPatrolPoint();
  updatePath();
  const patrolFollow: StateOptions = {
    id: StateNames.PATROL_FOLLOW,
    transitions: [],
    context,
    onUpdate(newState: State) {
      const {
        Math: {
          Distance: { Between },
        },
      } = Phaser;

      const [targetTile] = context.path;
      const distance = Between(
        entity.body.x,
        entity.body.y,
        targetTile[0],
        targetTile[1]
      );

      if (distance <= 1) {
        context.path.shift();
        newState.trigger(Triggers.START_TURNING);
        return;
      }

      if (context.path.length === 0) {
        nextPatrolPoint();
        updatePath();
      }

      const {
        scene: { player },
      } = entity;

      if (player.isAlive && canSeeTarget(entity, player)) {
        newState.trigger(Triggers.SEE_QUARY);
      } else {
        entity.locomotion.moveTowards(targetTile[0], targetTile[1]);
      }
    },
  };

  const patrol: StateOptions = {
    id: States.PATROL,
    children: [patrolFollow, patrolTurn],
    transitions: [
      {
        triggerId: Triggers.START_TURNING,
        toStateId: States.PATROL_TURN,
      },
      {
        triggerId: Triggers.FINISH_TURNING,
        toStateId: States.PATROL_FOLLOW,
      },
    ],
  };

  return patrol;
}
