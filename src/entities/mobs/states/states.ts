const States = {
  ALERT: 'alert',
  PURSUE: 'pursue',
  RELAXED: 'relaxed',
  SEARCH: 'search',
  WANDER: 'wander',
  PATROL: 'patrol',
  PATROL_TURN: 'patrol_turn',
  PATROL_FOLLOW: 'patrol_follow',
};

export default States;
