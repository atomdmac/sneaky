const Triggers = {
  ARRIVE: 'arrive',
  GET_BORED: 'get_bored',
  SEE_QUARY: 'see_quary',
  START_TURNING: 'start_turning',
  FINISH_TURNING: 'finish_turning',
};

export default Triggers;
