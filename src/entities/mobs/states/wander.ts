import { isWithinFieldOfView } from '@src/maps/collision';
import { State, StateOptions } from '@src/lib/state-machine';
import { hasLineOfSight } from '@src/maps/collision';
import { Entity } from '@src/entities/entity';
import StateNames from './states';
import Triggers from './triggers';

export type WanderContext = {
  quary: Entity | null;
};

export default function create(entity: Entity, defaultContext?: WanderContext) {
  const context = defaultContext || {
    quary: null,
  };

  const wander: StateOptions = {
    id: StateNames.WANDER,
    transitions: [],
    onUpdate: function (currentState: State) {
      const { quary } = context;

      // Prevent mob from getting stuck on walls and corners.
      const { direction, body } = entity;
      if (body.onWall() && (body.onFloor() || body.onCeiling())) {
        direction.bearing.x *= -1;
        direction.bearing.y *= -1;
      } else {
        if (body.onFloor() || body.onCeiling()) {
          direction.bearing.y = 0;
        }
        if (body.onWall()) {
          direction.bearing.x = 0;
        }
      }

      const bearingDirection = Math.random() < 0.5 ? -1 : 1;
      const MAX_DELTA = 0.2;
      const bearingDelta = Math.random() * MAX_DELTA * bearingDirection;
      entity.direction.bearing.setAngle(
        entity.direction.bearing.angle() + bearingDelta
      );
      entity.direction.bearing.setLength(1);
      entity.body.velocity
        .setFromObject(entity.direction.bearing)
        .setLength(entity.body.maxVelocity.length());

      // Only chase the quary if they're alive.
      if (quary?.isAlive) {
        const isWithinRange =
          entity.body.position.distance(quary) < entity.sight.distance;
        const canSeeTarget =
          isWithinRange &&
          hasLineOfSight(
            entity.body.position,
            quary.body.position,
            entity.scene.map
          ) &&
          isWithinFieldOfView(
            entity.body.position,
            entity.direction.bearing,
            entity.sight.fieldOfViewRadians,
            quary.body.position
          );
        if (canSeeTarget) {
          currentState.trigger(Triggers.SEE_QUARY);
        }
      }
    },
  };

  return wander;
}
