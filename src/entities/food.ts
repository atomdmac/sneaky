import {CollectableComponent, PositionComponent, VisibleComponent} from "../components";
import {Entity} from "./entity";

export class Food extends Entity {
  constructor () {
    super();

    const vis = new VisibleComponent(this);
    vis.character = '%';
    vis.foregroundColor = '#f0f0f0';

    const collectable = new CollectableComponent(this);
    collectable.type = 'food';

    this.addComponent(vis);
    this.addComponent(collectable);
    this.addComponent(new PositionComponent(this));
  }
}
