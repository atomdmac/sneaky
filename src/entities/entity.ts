import {some} from 'lodash';
import { AbstractComponent, IComponent, Modifier } from '../components';

export type ComponentConst<T> = {
  new(...args: any[]): T
}

export class Entity {
  _components: IComponent[] = [];

  hasComponent <C extends AbstractComponent<C>>(constr: ComponentConst<C>): boolean {
    return !!this._components.find((c) => c instanceof constr);
  }

  hasAllComponents (constrs: ComponentConst<any>[]): boolean {
    return constrs.every((cmpt: ComponentConst<any>) => this.hasComponent(cmpt));
  }

  getComponent <C extends AbstractComponent<C>>(constr: ComponentConst<C>): C | null {
    const component = this._components.find((c) => c instanceof constr);
    return component as C || null;
  }

  addComponent <C extends AbstractComponent<C>>(component: C): void {
    this._components.push(component);
  }

  removeComponent <C extends AbstractComponent<C>>(constr: ComponentConst<C>): void {
    this._components = this._components.filter((c) => c instanceof constr);
  }

  hasComponentModifier <C extends AbstractComponent<C>>(
    constr: ComponentConst<C>,
    id: string
  ): boolean {
    if (this.hasComponent(constr)) {
      const mods = this.getComponent(constr).mods;
      return some(mods, (mod: Modifier<C>) => mod.id === id);
    } else {
      return false;
    }
  }

  addComponentModifier <C extends AbstractComponent<C>>(
    constr: ComponentConst<C>,
    modifier: Modifier<C>
  ): void {
    const cmpt = this.getComponent(constr);
    cmpt.mods.push(modifier);
  }

  removeComponentModifier <C extends AbstractComponent<C>>(
    constr: ComponentConst<C>,
    id: string
  ): void {
    const cmpt = this.getComponent(constr);
    cmpt.mods = cmpt.mods.filter((mod: Modifier<C>) => mod.id !== id);
  }

  getComponentValue <C extends AbstractComponent<C>, T extends keyof C> (
    constr: ComponentConst<C>,
    prop: T,
  )  {
    const cmpt = this.getComponent(constr);
    const baseValue = cmpt[prop];
    const addMods = (prevValue: C[T], mod: Modifier<C>) => mod.value(prevValue, this);
    const modded = cmpt.mods.reduce(addMods, baseValue) as typeof baseValue;
    return modded;
  }

  setComponentValue <
    C extends AbstractComponent<C>,
    T extends keyof C>
    (constr: ComponentConst<C>, prop: T, value: C[T])  {
      this.getComponent(constr)[prop] = value;
   }
}
