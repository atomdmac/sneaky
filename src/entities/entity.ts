import Phaser, { GameObjects, Physics } from 'phaser';
import SneakyScene from '@src/scenes/i-sneaky-scene';
import config from '@src/config';
import { Inventory } from './inventory';

type CauseOfDeath = {
  entity?: Entity;
  description?: string;
};

interface DirectionalityComponent {
  // The direction the entity is pointing.
  direction: Directionality;
}

export class Directionality {
  public bearing: Phaser.Math.Vector2;
  constructor() {
    this.bearing = new Phaser.Math.Vector2();
    Phaser.Math.RandomXY(this.bearing);
  }
}

interface LocomotionComponent {
  locomotion: Locomotion;
}

type LocomotionGameObject = Phaser.GameObjects.GameObject & {
  body: Phaser.Physics.Arcade.Body;
  direction: Directionality;
};

export class Locomotion {
  constructor(
    public scene: Phaser.Scene,
    public gameObject: LocomotionGameObject,
    public baseSpeed: number
  ) {}

  /**
   * Move the entity towards an object or point.
   */
  public moveTowards(x: number, y: number): number {
    const {
      gameObject: { body },
      scene,
    } = this;
    const difference = Phaser.Math.Distance.Between(x, body.x, y, body.y);
    const speed =
      difference < body.maxVelocity.length()
        ? difference / 2
        : body.maxVelocity.length();

    const newAngle = scene.physics.moveTo(this.gameObject, x, y, speed);

    body.velocity.x = x - body.x;
    body.velocity.y = y - body.y;
    body.velocity.setLength(this.gameObject.body.maxVelocity.length());

    // Navigate around corners
    if (body.onFloor() || body.onCeiling()) {
      body.velocity.x = x - body.x;
      body.velocity.y = 0;
      body.velocity.setLength(body.maxVelocity.length());
    }
    if (body.onWall()) {
      body.velocity.x = 0;
      body.velocity.y = y - body.y;
      body.velocity.setLength(body.maxVelocity.length());
    }
    return newAngle;
  }
}

interface SightComponent {
  sight: Sight;
}
export class Sight {
  public distance: number;
  public fieldOfViewRadians: number;

  constructor(distance: number, fieldOfViewRadians: number) {
    this.distance = distance;
    this.fieldOfViewRadians = fieldOfViewRadians;
  }
}

export class Entity
  extends GameObjects.Container
  implements DirectionalityComponent, LocomotionComponent, SightComponent
{
  // Reference to the scene that this entity exists within.
  public declare scene: SneakyScene;
  // The representation of the entity in the physics simulation.
  public declare body: Physics.Arcade.Body;

  public direction: Directionality;
  public locomotion: Locomotion;
  public sight: Sight;
  public inventory: Inventory;

  private _isAlive: boolean;

  static EntityType = 'mob';
  protected _entityType: string;
  public get entityType(): string {
    return this._entityType;
  }

  constructor(
    scene: SneakyScene,
    x: number,
    y: number,
    children?: GameObjects.GameObject[],
    baseSpeed = 100,
    dragMultiplier = 8
  ) {
    super(scene, x, y, children);

    this._entityType = Entity.EntityType;
    this._isAlive = true;

    // Physics
    scene.physics.add.existing(this);
    this.body.onCollide = true;
    this.body.setSize(config.tiles.width, config.tiles.height);
    this.body.maxVelocity.setLength(baseSpeed);
    this.body.setAllowDrag(true);
    this.body.setDrag(baseSpeed * dragMultiplier, baseSpeed * dragMultiplier);
    this.body.setCollideWorldBounds(true, 0, 0, true);

    this.direction = new Directionality();
    this.locomotion = new Locomotion(this.scene, this, baseSpeed);
    this.sight = new Sight(100, 1.5);
    this.inventory = new Inventory({ owner: this });

    const particleManager = scene.add.particles('mob');

    this.on('die', () => {
      particleManager.createEmitter({
        follow: this,
        maxParticles: 5,
        frequency: 1,
        speed: {
          min: 100,
          max: 200,
        },
        scale: {
          min: 0.1,
          max: 0.25,
        },
        lifespan: 150,
      });

      scene.tweens.killTweensOf(this);
      scene.tweens.add({
        targets: this,
        props: {
          scale: 2,
          alpha: 0,
        },
        duration: 200,
      });
    });

    // TODO: Only turn on debug pointer when configuration allows it.
    const { tileWidth, tileHeight } = scene.map;
    const hitArea = new Phaser.Geom.Rectangle(0, 0, tileWidth, tileHeight);
    this.setInteractive(hitArea, Phaser.Geom.Rectangle.Contains);
    this.on('pointerdown', () => {
      console.log(this.name, this);
    });
  }

  get isAlive() {
    return this._isAlive;
  }

  /**
   * Cause the entity to die.
   */
  public die(cause?: CauseOfDeath) {
    this.locomotion.gameObject.setActive(false);
    this.body.setEnable(false);
    this.setAlpha(0.5);
    this._isAlive = false;
    this.emit('die', cause);
  }
}
