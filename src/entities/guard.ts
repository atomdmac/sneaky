import { Entity} from "./entity";
import {
  FieldOfViewComponent,
  FollowPathComponent,
  VisibleComponent,
  PositionComponent,
  SpeedComponent,
  ImpassableComponent,
  BearingComponent,
  AbstractComponent
} from '../components';
import {FieldOfHearingComponent, FieldOfSoundComponent} from "../systems/field-of-hearing";

export class GuardAIComponent extends AbstractComponent<GuardAIComponent> { }

export class Guard extends Entity {
  name: string;

  constructor () {
    super();
    this.name = Math.random() + '';
    this.addComponent(new GuardAIComponent(this));
    this.addComponent(new SpeedComponent(this));
    this.addComponent(new FollowPathComponent(this));
    this.addComponent(new PositionComponent(this));
    this.addComponent(new ImpassableComponent(this));
    this.addComponent(new BearingComponent(this));

    const sound = new FieldOfSoundComponent(this);
    sound.radius = 10;
    this.addComponent(sound);

    const hearing = new FieldOfHearingComponent(this);
    hearing.radius = 0;
    this.addComponent(hearing);

    const fov = new FieldOfViewComponent(this);
    fov.radius = 3;
    this.addComponent(fov);

    const vis = new VisibleComponent(this);
    vis.character = 'G';
    this.addComponent(vis);
  }
}
