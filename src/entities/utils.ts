import { every } from 'lodash';
import { Entity } from './entity';
import { PositionComponent } from '../components';

export function getEntitiesAt (
  entities: Entity[],
  x: number,
  y: number,
  withComponents: any[] = []
): Entity[] {
  return entities.filter((entity: Entity) => {
    const pos = entity.getComponent(PositionComponent);
    const correctType = !withComponents.length
      || every(
        withComponents, 
        (c: any) => entity.hasComponent(c)
      );
    return pos && pos.x === x && pos.y === y && correctType;
  });
}

export function getDistanceBetween(
  x1: number,
  y1: number,
  x2: number,
  y2: number
): number {
  const a = Math.abs(x1 - x2);
  const b = Math.abs(y1 - y2);
  return Math.sqrt((a * a) + (b * b))
}
