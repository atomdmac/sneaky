import { Entity } from './entity';
import Item from './items/item';

export class Inventory {
  owner: Entity;
  protected items: Item[] = [];

  constructor({ owner }: { owner: Entity }) {
    this.owner = owner;
  }

  preUpdate(time: number, delta: number) {
    if (this.canUpdate()) {
      this.items.forEach((i) => i.preUpdate(time, delta));
    }
  }

  postUpdate(time: number, delta: number) {
    if (this.canUpdate()) {
      this.items.forEach((i) => i.postUpdate(time, delta));
    }
  }

  update(time: number, delta: number) {
    if (this.canUpdate()) {
      console.log(`Updating ${this.items.length} items.`);
      this.items.forEach((i) => i.update(time, delta));
    }
  }

  add(item: Item) {
    if (!this.items.includes(item)) {
      item.pickUp(this.owner);
      this.items.push(item);
    }
  }

  remove(item: Item) {
    this.items = this.items.filter((i) => i !== item);
  }

  clear() {
    // If item hasn't been destroyed by some other means, drop it now.
    this.items.forEach((item) => item.scene && item.drop());
    this.items.splice(0, this.items.length);
  }

  size() {
    return this.items.length;
  }

  getItemAt(index: number) {
    index = Math.max(0, index);
    index = Math.min(index, this.items.length - 1);

    return this.items[index];
  }

  protected canUpdate() {
    return !this.owner.scene.physics.world.isPaused;
  }
}
