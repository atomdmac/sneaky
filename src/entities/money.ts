import {CollectableComponent, PositionComponent, VisibleComponent} from "../components";
import {Entity} from "./entity";

export class Money extends Entity {
  constructor () {
    super();

    const vis = new VisibleComponent(this);
    vis.character = '$';
    vis.foregroundColor = '#fff000';

    const collectable = new CollectableComponent(this);
    collectable.type = 'money';

    this.addComponent(vis);
    this.addComponent(collectable);
    this.addComponent(new PositionComponent(this));
  }
}
