import { GameObjects } from 'phaser';
import { Entity } from '../entity';
import Item, { ItemEvent } from './item';

export default function create(
  ...args: ConstructorParameters<typeof GameObjects.Container>
) {
  const item = new Item(
    {
      durability: 100,
      useDuration: 100,
      cooldownDuration: 0,
    },
    ...args
  );

  // Set size to aid with physics stuff.
  item.width = 16;
  item.height = 16;

  // Add an icon to represent this sprite.
  const sprite = item.scene.make.sprite(
    {
      key: 'dash-icon',
    },
    false
  );

  // Adjust durability display placement.
  item.add(sprite);
  item.durabilityDisplay.setHeight(4);
  item.durabilityDisplay.setWidth(16);
  item.durabilityDisplay.x = -6;
  item.durabilityDisplay.y = 8;

  const mgr = item.scene.add.particles('player');
  const emitter = mgr.createEmitter({
    maxParticles: 10,
    lifespan: 75,
    alpha: 0.2,
    follow: item,
    followOffset: {
      x: 16,
      y: 16,
    },
  });
  emitter.stop();

  item.events.on(ItemEvent.PICKED_UP, () => {
    console.log('PICKED_UP!');
  });

  item.events.on(ItemEvent.USE, () => {
    if (item.user) {
      emitter.start();
      emitter.follow = item.user;
    }
  });

  item.events.on(ItemEvent.PRE_UPDATE, () => {
    if (item.user && item.getEngaged()) {
      const user = item.user as Entity;
      const { baseSpeed } = user.locomotion;

      const newSpeed = baseSpeed * 3;
      user.body.maxVelocity.setLength(newSpeed);
    }
  });

  item.events.on(ItemEvent.POST_UPDATE, () => {
    if (item.user && item.getEngaged()) {
      const user = item.user as Entity;
      const { baseSpeed } = user.locomotion;

      const newSpeed = baseSpeed * 3;
      user.body.maxVelocity.setLength(newSpeed);
      user.body.velocity.setLength(newSpeed);
    }
  });

  item.events.on(ItemEvent.COOLDOWN_STARTED, () => {
    emitter.stop();
  });

  item.events.on(ItemEvent.SPENT, () => {
    emitter.stop();
    item.destroy();
  });
  item.on(Phaser.GameObjects.Events.DESTROY, () => {
    emitter.stop();
    emitter.killAll();
  });

  return item;
}
