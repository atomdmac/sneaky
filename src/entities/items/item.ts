import Phaser, { Events, GameObjects } from 'phaser';
import ProgressBar from '@src/ui/progress-bar';

export type ItemConfig = {
  useDuration: number;
  cooldownDuration: number;
  durability: number;
  engagementConditions?: (() => boolean)[];
};

export enum ItemEvent {
  USE = 'USE',
  USING = 'USING',
  ENGAGE = 'ENGAGE',
  DISENGAGE = 'DISENGAGE',
  SPENT = 'SPENT',
  PICKED_UP = 'PICKED_UP',
  DROPPED = 'DROPPED',
  COOLDOWN_STARTED = 'COOLDOWN_STARTED',
  COOLDOWN_ENDED = 'COOLDOWN_ENDED',

  PRE_UPDATE = 'PRE_UPDATE',
  UPDATE = 'UPDATE',
  POST_UPDATE = 'POST_UPDATE',
}

export default class Item extends GameObjects.Container {
  public events: Phaser.Events.EventEmitter;
  public user: Phaser.GameObjects.GameObject | null;
  public durabilityDisplay: ProgressBar;
  public declare body: Phaser.Physics.Arcade.Body;

  protected isEngaged: boolean;
  protected _durability: number;
  protected _currentDurability: number;
  protected cooldownDuration: number;
  protected currentCooldownDuration: number;
  protected useDuration: number;
  protected currentUseDuration: number;
  protected engagementConditions: (() => boolean)[];

  constructor(
    config: ItemConfig,
    ...rest: ConstructorParameters<typeof GameObjects.Container>
  ) {
    super(...rest);

    const {
      durability,
      cooldownDuration,
      useDuration,
      engagementConditions = [],
    } = config;

    this.isEngaged = false;
    this._durability = durability;
    this._currentDurability = durability;
    this.cooldownDuration = cooldownDuration;
    this.currentCooldownDuration = 0;
    this.useDuration = useDuration;
    this.currentUseDuration = 0;
    this.engagementConditions = engagementConditions;
    this.events = new Events.EventEmitter();
    this.user = null;

    this.durabilityDisplay = new ProgressBar({
      width: 16,
      height: 8,
      x: -8,
      y: 8,
      scene: this.scene,
    });
    this.durabilityDisplay.setVisible(false);
    this.add(this.durabilityDisplay);
  }

  get durability() {
    return this._durability;
  }

  get currentDurability() {
    return this._currentDurability;
  }

  get isCoolingDown(): boolean {
    return !!this.currentCooldownDuration;
  }

  setEngaged(flag: boolean) {
    // Only run function if new state is different than current.
    if (flag === this.isEngaged) {
      return;
    }

    // Do not engage if item is spent or cooling down.
    if (this.isCoolingDown) {
      return;
    }

    // If attempting to engage...
    if (flag) {
      // Do not engage if the item is spent.
      if (this._currentDurability <= 0) {
        return;
      }

      // Do not engage if any engagement conditions are not met.
      if (this.engagementConditions.some((ec) => !ec())) {
        return;
      }
    }

    this.isEngaged = flag;
    this.currentUseDuration = 0;

    const event = flag ? ItemEvent.ENGAGE : ItemEvent.DISENGAGE;
    this.events.emit(event);
  }

  getEngaged(): boolean {
    return this.isEngaged;
  }

  pickUp(user: Phaser.GameObjects.GameObject) {
    this.user = user;
    this.body.setEnable(false);
    this.events.emit(ItemEvent.PICKED_UP, this.user);
  }

  drop() {
    this.user = null;
    this.setActive(true);
    this.setVisible(true);
    this.body.setEnable(true);
    this.events.emit(ItemEvent.DROPPED, this.user);
  }

  use() {
    // Item already in use.
    if (this.getEngaged()) {
      return;
    }

    // Item is cooling down.
    if (this.isCoolingDown) {
      return;
    }

    if (this._currentDurability <= 0) {
      return;
    }

    console.log('ATTEMPT TO USE');
    this.setEngaged(true);
    this.handleTick(16); // 60 fps = 16ms per tick
    this.events.emit(ItemEvent.USE);
  }

  private startCooldown() {
    // Start cooldown (if not started).
    if (!this.isCoolingDown) {
      this.currentUseDuration = 0;
      this.currentCooldownDuration = 1;
      this.setEngaged(false);
      this.events.emit(ItemEvent.COOLDOWN_STARTED);
    }
  }

  private clearDurations() {
    this.currentUseDuration = 0;
    this.currentCooldownDuration = 0;
  }

  /**
   * Fired on each game tick that the item is in it's cooldown phase.
   */
  private handleCooldownTick(delta: number) {
    this.currentCooldownDuration += delta;

    if (this.currentCooldownDuration >= this.cooldownDuration) {
      this.clearDurations();
      this.events.emit(ItemEvent.COOLDOWN_ENDED);
    }
  }

  /**
   * Fired on each game tick that this item is engaged/in use.
   */
  private handleUseTick(delta: number) {
    // Use the item.
    this._currentDurability -= 1;
    this.currentUseDuration += delta;
    this.events.emit(ItemEvent.USING);

    // Item is spent.
    if (this._currentDurability <= 0) {
      this.setEngaged(false);
      this.events.emit(ItemEvent.SPENT);
    }

    // Item reached max use duration and should be disengaged.
    else if (this.currentUseDuration >= this.useDuration) {
      this.setEngaged(false);
      this.startCooldown();
    }
  }

  /**
   * Fired on every game tick
   */
  private handleTick(delta: number) {
    // Item is cooling down.
    if (this.isCoolingDown) {
      this.handleCooldownTick(delta);
      return;
    }

    // Item not currently engaged.
    if (!this.getEngaged()) {
      return;
    }

    this.handleUseTick(delta);
  }

  protected canUpdate(): boolean {
    return !this.scene?.physics.world.isPaused;
  }

  update(time: number, delta: number) {
    // The item cannot update if time is stopped.
    if (!this.canUpdate()) {
      return;
    }
    super.update(time, delta);
    this.handleTick(delta);
    this.events.emit(ItemEvent.UPDATE, time, delta);

    // Update durability display.
    this.durabilityDisplay.setCurrent(this.currentDurability);
    this.durabilityDisplay.setTotal(this.durability);
  }

  preUpdate(time: number, delta: number) {
    // The item cannot update if time is stopped.
    if (!this.canUpdate()) {
      return;
    }
    this.events.emit(ItemEvent.PRE_UPDATE, time, delta);
  }

  postUpdate(time: number, delta: number) {
    // The item cannot update if time is stopped.
    if (!this.canUpdate()) {
      return;
    }
    this.events.emit(ItemEvent.POST_UPDATE, time, delta);
  }
}
