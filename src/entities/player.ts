import { Entity } from './entity';
import { AliveComponent, CollectableComponent, CollectorComponent, ConsumerComponent, DoorUserComponent, FollowPathComponent, ImpassableComponent, SpeedComponent } from '../components';
import {
  KeyboardControlComponent,
  FieldOfViewComponent,
  PositionComponent,
  VisibleComponent,
} from '../components';
import {Food} from './food';
import {Money} from './money';
import {FieldOfHearingComponent, FieldOfSoundComponent} from '../systems/field-of-hearing';

export class Player extends Entity {
  constructor () {
    super();

    const collectorCmpt = new CollectorComponent(this);
    const food = new Food();
    food.getComponent(CollectableComponent).count = 20;
    const money = new Money();
    money.getComponent(CollectableComponent).count = 0;

    collectorCmpt.collectables.push(food);
    collectorCmpt.collectables.push(money);
    this.addComponent(collectorCmpt);

    const hearing = new FieldOfHearingComponent(this);
    this.addComponent(hearing);

    const sound = new FieldOfSoundComponent(this);
    this.addComponent(sound);

    const fov = new FieldOfViewComponent(this);
    this.addComponent(fov);

    this.addComponent(new SpeedComponent(this));
    this.addComponent(new FollowPathComponent(this));
    this.addComponent(new KeyboardControlComponent(this))
    this.addComponent(new VisibleComponent(this));
    this.addComponent(new PositionComponent(this));
    this.addComponent(new DoorUserComponent(this));
    this.addComponent(new ImpassableComponent(this));
    this.addComponent(new AliveComponent(this));
    this.addComponent(new ConsumerComponent(this));
  }
}
