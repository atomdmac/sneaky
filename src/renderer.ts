import { Display } from './interfaces';

export class Renderer {
  displays: {
    [s: string]: Display
  }

  stage: Element;

  constructor () {
    this.displays = {};
    this.stage = document.querySelector('#stage');
  }

  addDisplay (name: string, display: Display) {
    const layer = document.createElement('div');
    layer.setAttribute('class', 'layer');
    layer.appendChild(display.getContainer());

    this.displays[name] = display;
    this.stage.appendChild(layer);
  }

  getDisplay (name: string):Display {
    return this.displays[name];
  }

  clearAll () {
    Object.keys(this.displays)
      .map(displayName => this.displays[displayName])
      .forEach(display => {
        const {
          width, tileWidth,
          height, tileHeight
        } = display.getOptions();
        display.getContainer().getContext('2d').clearRect(
          0, 0, width * tileWidth, height * tileHeight
        );
      })
  }
}
