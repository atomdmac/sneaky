import { Game } from '../game';
import { GameState } from '../interfaces';
import { Renderer } from '../renderer';
import { isHighScore, getScore, setScore, Score } from '../high-score';

export class LoseState implements GameState {
  renderer: Renderer;
  game: Game;
  isHighScore: boolean;
  newScore: Score;
  oldScore: Score;

  constructor (renderer: Renderer, game: Game) {
    this.renderer = renderer;
    this.game = game;
  }

  start () {
    const stats = this.game.world.stats;

    this.newScore = {
      date: Date.now(),
      loot: stats.loot,
      floor: stats.floorsClimbed
    };
    this.oldScore = getScore();
    this.isHighScore = isHighScore(this.newScore);

    if (this.isHighScore) {
      setScore(this.newScore);
    }

    this.renderer.clearAll();
    this.render();
  }

  stop () {}

  update () {
    this.handleInput();
  }

  handleInput () {
    if (this.game.keyboard.keyIsDown(' ')) {
      this.game.world.reset();
      this.game.setState('play');
    };
  }

  render () {
    const highScoreMessage = this.isHighScore
        ? `* * New Record! * *`
        : 'You\'ve had better days...';

    const messages = [
      '!!! BUSTED !!!',
      '',
      `Score: ${this.newScore.loot + this.newScore.floor}`,
      '',
      highScoreMessage,
      '',
      'Press SPACE to continue'
    ];

    const {
      width, height
    } = this.renderer.getDisplay('main').getOptions();
    this.renderer.getDisplay('main').clear();

    messages.forEach((message, index) => {
      const x = Math.floor((width - message.length) / 2);
      const y = Math.floor((height - messages.length) / 2) + index;
      this.renderer.getDisplay('main').drawText(
        x, y, message, '#fff', '#000'
      );
    });
  }
}
