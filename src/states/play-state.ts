import { Game } from '../game';
import { GameState } from '../interfaces/game-state';
import { Renderer } from '../renderer';
import { World } from '../world';
import Settings from '../settings';
import { Entity } from '../entities';
import {
  AliveComponent,
  CollectableComponent,
  CollectorComponent,
  DoorComponent,
  DoorUserComponent,
  FieldOfViewComponent,
  KeyboardControlComponent,
  PositionComponent,
  SpeedComponent,
  VisibleComponent 
} from '../components';
import {
  DoorsSystem,
  FieldOfViewSystem,
  FollowPathSystem,
  CollectorSystem,
  ConsumerSystem,
  getCollectableCount,
  canSee, 
  couldSee,
  moveTo,
  stopConsuming,
  getCollectable,
  startConsuming
} from '../systems';
import { getEntitiesAt } from '../entities/utils';
import {GuardAISystem} from '../systems/guard-ai';
import {canHear, FieldOfHearingComponent, FieldOfHearingSystem, FieldOfSoundComponent} from '../systems/field-of-hearing';

export class PlayState implements GameState {
  currentActor: Entity;
  renderer: Renderer;
  game: Game;
  world: World;
  hudPadding = 3;

  systems: {
    collector: CollectorSystem,
    consumer: ConsumerSystem,
    doors: DoorsSystem,
    hearing: FieldOfHearingSystem,
    fov: FieldOfViewSystem,
    guardAI: GuardAISystem,
    path: FollowPathSystem,
  }

  constructor(renderer: Renderer, game: Game) {
    this.renderer = renderer;
    this.game = game;

    this.systems = {
      collector: new CollectorSystem(),
      consumer: new ConsumerSystem(),
      doors: new DoorsSystem(),
      fov: new FieldOfViewSystem(),
      hearing: new FieldOfHearingSystem(),
      guardAI: new GuardAISystem(),
      path: new FollowPathSystem()
    };
  }

  start () {
    this.game.world.nextFloor();

    // Render the initial screen.
    this.updateSystems(this.game.world.player);
    this.render();
  }

  stop () { }

  update () {
    const currentEntity = this.game.scheduler.getCurrent();

    if (currentEntity.hasComponent(KeyboardControlComponent)) {
      const playerActed = this.handleInput();
      if (playerActed) {
        this.updateSystems(currentEntity);

        // Update all other entities.
        this.game.scheduler.next();
        this.updateScheduled();
      }
    } else {
      this.updateScheduled();
    }

    const playerAlive = this.game.world.player.getComponent(AliveComponent);
    if (!playerAlive.isAlive) {
      this.game.setState('lose');
    }

    this.game.world.purgeMarkedEntities();
  }

  updateScheduled () {
    // TODO: Dude gross.
    let nextEntity = this.game.scheduler.getCurrent();
    while (!nextEntity.hasComponent(KeyboardControlComponent)) {
      this.updateSystems(nextEntity);
      this.game.scheduler.next();
      nextEntity = this.game.scheduler.getCurrent();
    }
  }

  updateSystems (currentEntity: Entity) {
    this.systems.fov.update(currentEntity, this.game)
    this.systems.hearing.update(currentEntity, this.game)
    this.systems.guardAI.update(currentEntity, this.game);
    this.systems.path.update(currentEntity, this.game);
    this.systems.doors.update(currentEntity, this.game);
    this.systems.collector.update(currentEntity, this.game);
    this.systems.consumer.update(currentEntity);
    this.systems.fov.update(currentEntity, this.game)
  }

  drawEntities (drawList: Entity[]) {
    drawList.forEach((e, i) => {
      this.drawEntity(e);
    });
  }

  drawEntity (e: Entity) {
    const pos = e.getComponent(PositionComponent);
    const visible = e.getComponent(VisibleComponent);
    const playerCanSee = canSee(
      this.game.world.player,
      pos.x,
      pos.y, 
      this.game.world.floor
    );
    if (!Settings.fogOfWar || visible && playerCanSee) {
      this.renderer
        .getDisplay('main')
        .draw(
          pos.x,
          pos.y + this.hudPadding, 
          visible.character, 
          visible.foregroundColor, 
          visible.backgroundColor, 
        );
    }
  }

  drawFOV (entities: Entity[]) {
    const {
      width,
      height,
      tileWidth,
      tileHeight
    } = this.renderer.getDisplay('effects').getOptions();
    const context = this.renderer.getDisplay('effects').getContainer().getContext('2d');
    context.clearRect(0, 0, width * tileWidth, height * tileHeight);
    context.globalAlpha = 0.5;
    entities.forEach(e => {
      if (e === this.game.world.player) {
        return;
      }
      const fov = e.getComponent(FieldOfViewComponent);
      if (fov) {
        fov.current.forEach(cell => {
          const playerSees = canSee(
            this.game.world.player,
            cell.x,
            cell.y,
            this.game.world.floor
          );
          if (!Settings.fogOfWar || playerSees) {
            this.renderer
            .getDisplay('effects')
            .draw(cell.x, cell.y + this.hudPadding, '', '#aa0', '#aa0');
          }
        })
      }
    });
  }

  drawHeard () {
    const player = this.game.world.player
    const hears = player.hasComponent(FieldOfHearingComponent)
      && player.getComponentValue(FieldOfHearingComponent, 'current');
    if (hears) {
      hears.forEach((entity: Entity) => {
        const pos = entity.getComponent(PositionComponent);
        this.renderer
          .getDisplay('effects')
          .draw(pos.x, pos.y + this.hudPadding, '', '#aa0', '#aa0');
      });
    }
  }

  drawHUD () {
    // Running Status
    // const playerSpeed = this.game.world.player.getComponent(SpeedComponent);
    const playerIsRunning = this.game.world.player.hasComponentModifier(SpeedComponent, 'running');
    this.renderer
      .getDisplay('main')
      .drawText(
        0, 2,
        playerIsRunning
          ? '%c{red}Running'
          : '%c{grey}Sneaking',
        '#000',
        '#000'
      );

    // Money
    const moneyCount = getCollectableCount(this.game.world.player, 'money');
    this.renderer
      .getDisplay('main')
      .drawText(0, 0, `%c{green}$: %c{}${moneyCount}`, '#fff', '#000');

    // Food
    const foodCount = getCollectableCount(this.game.world.player, 'food');
    this.renderer
      .getDisplay('main')
      .drawText(
        0, 1,
        `%c{yellow}%: %c{}${foodCount}`,
        '#fff',
        '#000'
      );

    // Floors climbed
    this.renderer
      .getDisplay('main')
      .drawText(20, 0, `Floor: ${this.game.world.stats.floorsClimbed}`, '#fff', '#000');
  }

  render () {
    // Start phresh!
    this.renderer.clearAll();

    // Redraw HUD.
    this.drawHUD();

    // Redraw map.
    this.game.world.floor.cells.forEach(c => {
      const currentlySees = canSee(this.game.world.player, c.x, c.y, this.game.world.floor);
      const alreadySaw = couldSee(this.game.world.player, c.x, c.y, this.game.world.floor);

      if (!Settings.fogOfWar || currentlySees) {
        this.renderer
          .getDisplay('main')
          .DEBUG(c.x, c.y + this.hudPadding, c.passable);
      } else if (alreadySaw) {
        const color = c.passable
          ? '#333'
          : '#000';
        this.renderer
          .getDisplay('main')
          .draw(c.x, c.y + this.hudPadding, '!', color, color);
      } else {
        this.renderer
          .getDisplay('main')
          .DEBUG(c.x, c.y + this.hudPadding, false);
      }
    });

    this.drawEntities(this.game.world.entities);
    this.drawFOV(this.game.world.entities);
    this.drawHeard();
  }

  handleInput (): boolean {
    const keyboard = this.game.keyboard;
    const player = this.game.world.player;
    let hasActed = false;
    console.log('Handing input...', this.game.world.player);

    const foodCollectable = getCollectable(player, 'food');
    const foodCount = foodCollectable.getComponent(CollectableComponent).count;

    player.removeComponentModifier(FieldOfSoundComponent, 'movement');
    const applyMovementMods = () => player.addComponentModifier(FieldOfSoundComponent, {
      id: 'movement',
      field: 'radius',
      value: (val: number) => val + 1
    });
    

    if (keyboard.keyIsDown('h') || keyboard.keyIsDown(4)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'WEST', this.game.world.floor);
      if (hasActed) applyMovementMods();
    } else if (keyboard.keyIsDown('y') || keyboard.keyIsDown(7)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'NORTH_WEST', this.game.world.floor);
      if (hasActed) applyMovementMods();
    } else if (keyboard.keyIsDown('j') || keyboard.keyIsDown(2)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'SOUTH', this.game.world.floor);
      if (hasActed) applyMovementMods();
    } else if (keyboard.keyIsDown('b') || keyboard.keyIsDown(1)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'SOUTH_WEST', this.game.world.floor);
      if (hasActed) applyMovementMods();
    } else if (keyboard.keyIsDown('k') || keyboard.keyIsDown(8)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'NORTH', this.game.world.floor);
      if (hasActed) applyMovementMods();
    } else if (keyboard.keyIsDown('l') || keyboard.keyIsDown(6)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'EAST', this.game.world.floor);
      if (hasActed) applyMovementMods();
    } else if (keyboard.keyIsDown('n') || keyboard.keyIsDown(3)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'SOUTH_EAST', this.game.world.floor);
      if (hasActed) applyMovementMods();
    } else if (keyboard.keyIsDown('u') || keyboard.keyIsDown(9)) {
      if (foodCount < 1) return;
      hasActed = moveTo(player, 'NORTH_EAST', this.game.world.floor);
      if (hasActed) applyMovementMods();
    }

    // Use doors.
    else if (keyboard.keyIsDown('>')) {
      const playerPos = player.getComponent(PositionComponent);
      const doors = getEntitiesAt(
        this.game.world.entities, 
        playerPos.x,
        playerPos.y,
        [DoorComponent]
      ).map((d: Entity) => d.getComponent(DoorComponent));
      if (doors.length) {
        const doorUser = player.getComponent(DoorUserComponent);
        doorUser.using = doors[0];
        hasActed = true;
      }
    }

    // Run / Sneak
    else if (keyboard.keyIsDown('r')) {
      if (player.hasComponentModifier(SpeedComponent, 'running')) {
        player.removeComponentModifier(SpeedComponent, 'running');
        player.removeComponentModifier(FieldOfSoundComponent, 'running');
        stopConsuming(player, foodCollectable);
      } else {
        player.addComponentModifier(SpeedComponent, {
          id: 'running',
          field: 'base',
          value: (val: number) => val + 1
        });
        player.addComponentModifier(FieldOfSoundComponent, {
          id: 'running',
          field: 'radius',
          value: (val: number, entity: Entity) => {
            if (entity.hasComponentModifier(FieldOfSoundComponent, 'movement')) {
              return val + 5
            } else {
              return val;
            }
          }
        });
        startConsuming(player, foodCollectable, -1);
      }
    }

    // Pick stuff up
    else if (keyboard.keyIsDown('g')){
      const playerCollector = player.getComponent(CollectorComponent);
      const playerPos = player.getComponent(PositionComponent);
      const collectablesHere = getEntitiesAt(
        this.game.world.entities,
        playerPos.x,
        playerPos.y,
        [CollectableComponent]
      );
      if (collectablesHere.length) {
        playerCollector.collecting = collectablesHere[0];
        hasActed = true;
      }
    }

    // Wait a turn.
    else if (keyboard.keyIsDown('.') || keyboard.keyIsDown(5)) {
      hasActed = true;
    }

    return hasActed;
  }
}
