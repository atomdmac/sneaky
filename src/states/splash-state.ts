import { Game } from '../game';
import { GameState } from '../interfaces';
import { Renderer } from '../renderer';

export class SplashState implements GameState {
  renderer: Renderer;
  game: Game;

  constructor (renderer: Renderer, game: Game) {
    this.renderer = renderer;
    this.game = game;
  }

  start () {
    this.renderer.clearAll();
    this.render();
  }

  stop () {}

  update () {
    this.handleInput();
  }

  handleInput () {
    if (this.game.keyboard.keyIsDown(' ')) {
      this.game.setState('play');
    };
  }

  render () {
    const messages = [
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '',
      'S N E A K Y',
      '',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '* * * * * * * * * * * *',
      '',
      'Press SPACE to begin'
    ];

    const {
      width, height
    } = this.renderer.getDisplay('main').getOptions();
    this.renderer.getDisplay('main').clear();

    messages.forEach((message, index) => {
      const x = Math.floor((width - message.length) / 2);
      const y = Math.floor((height - messages.length) / 2) + index;
      this.renderer.getDisplay('main').drawText(
        x, y, message, '#fff', '#000'
      );
    });
  }
}
