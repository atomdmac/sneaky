import { GameState } from './interfaces/game-state';
import { Keyboard } from './keyboard';
import GameSettings from './settings';
import { World } from './world';
import { InitiativeTracker } from './scheduler';

export class Game {
  public keyboard: Keyboard;
  public world: World;
  public scheduler: InitiativeTracker;

  private paused: boolean;
  private state: GameState;
  private states: {
    [s: string]: GameState
  }

  constructor () {
    this.keyboard = new Keyboard();
    this.scheduler = new InitiativeTracker();
    this.paused = true;
    this.states = {};

    // Create the world.
    this.world = new World(
      this,
      GameSettings.width,
      GameSettings.height
    );

    // HACK: Scope `tick` to this instance.
    this.tick = this.tick.bind(this);

    this.keyboard.activate();
    this.tick();

    // TODO: Is there a better way to handle tick rate?
    document.addEventListener('keydown', this.tick);
  }

  pause () {
    this.paused = true;
  }

  unpause () {
    this.paused = false;
  }

  private tick () {
    this.update();
    this.render();
  }

  private update () {
    if (!this.paused && this.state) {
      this.state.update();
    }
  }

  private render () {
    if (this.state) {
      this.state.render();
    }
  }

  setState (stateName: string) {
    if (!this.states[stateName]) {
      throw new Error(`There is no state with the name ${stateName} within the game.`);
    } else {
      // If there is a current state, stop it before activating the new one.
      if (this.state) {
        this.state.stop();
      }
      this.state = this.states[stateName];
      this.state.start(this.world);
    }
  }

  addState (stateName: string, state: GameState) {
    this.states[stateName] = state;
  }
}
