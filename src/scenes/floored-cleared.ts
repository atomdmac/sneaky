import { gamepadButtonDown } from '@src/lib/gamepad';
import { Input } from 'phaser';

const {
  Keyboard: { KeyCodes },
} = Input;

type Keys = {
  restart: Phaser.Input.Keyboard.Key;
};

export default class FloorCleared extends Phaser.Scene {
  declare keys: Keys;

  create() {
    const { width, height } = this.renderer;

    const graphics = this.add.graphics();

    const color = 0xff0000;
    const alpha = 0.5;

    graphics.fillStyle(color, alpha);
    graphics.fillRect(0, 0, width, height);

    const message = this.add.text(0, 0, 'Floor Cleared!');
    message.setFontSize(100);
    message.setAlign('center');
    message.setColor('00ff00');
    message.setX(width / 2 - message.width / 2);
    message.setY(height / 2 - message.height / 2);

    message.setAlpha(0);
    graphics.setAlpha(0);
    this.tweens.add({
      targets: [message, graphics],
      alpha: 1,
      duration: 500,
    });

    this.keys = {
      restart: this.input.keyboard.addKey(KeyCodes.ENTER),
    };
  }

  restartGame() {
    this.scene.start('home');
  }

  update() {
    if (this.keys.restart.isDown || gamepadButtonDown(this, 1)) {
      this.restartGame();
    }
  }
}
