import config from '@src/config';
import { createMobs, Mob } from '@src/entities/mobs';
import { createPlayer, Player } from '@src/entities/player';
import { AudibleManager } from '@src/lib/audible';
import { isWithinFieldOfView } from '@src/maps/collision';
import createMap from '@src/maps/random-floor-map';
import { AStarFinder } from 'astar-typescript';
import Phaser, { GameObjects, Input } from 'phaser';
import ISneakyScene from './i-sneaky-scene';
import createDash from '../entities/items/dash';
import Item from '@src/entities/items/item';
import HomeUI from './home-ui';
import { getRandomSpawnPoints } from '@src/maps/spawn';
const {
  Keyboard: { KeyCodes },
} = Input;

type Keys = {
  up: Phaser.Input.Keyboard.Key;
  down: Phaser.Input.Keyboard.Key;
  left: Phaser.Input.Keyboard.Key;
  right: Phaser.Input.Keyboard.Key;
  wait: Phaser.Input.Keyboard.Key;
  use: Phaser.Input.Keyboard.Key;
  dash: Phaser.Input.Keyboard.Key;
};

export default class Home extends Phaser.Scene implements ISneakyScene<Keys> {
  declare player: Player;
  declare map: Phaser.Tilemaps.Tilemap;
  declare keys: Keys;
  declare astar: AStarFinder;
  declare audible: AudibleManager;
  declare items: Phaser.Physics.Arcade.Group;

  constructor(config: Phaser.Types.Scenes.SettingsConfig) {
    super(config);
  }

  preload() {
    this.load.image('player', 'assets/player.png');
    this.load.image('mob', 'assets/mob.png');
    this.load.image('door', 'assets/door.png');
    this.load.image('dirt_1.png', 'assets/dirt_1.png');
    this.load.image('stone_1.png', 'assets/stone_1.png');
    this.load.image('exclaimation-point', 'assets/exclaimation-point.png');
    this.load.image('status-slow', 'assets/status-slow.png');
    this.load.image('status-wait', 'assets/status-wait.png');
    this.load.image('dash-icon', 'assets/dash-icon.png');
  }

  create() {
    this.scene.launch('home-ui');
    this.audible = new AudibleManager(this);

    // Map
    const { tileMap, astar } = createMap(this, 'current-floor');
    this.map = tileMap;
    this.astar = astar;

    // Physics
    this.physics.world.setBounds(
      0,
      0,
      this.map.widthInPixels,
      this.map.heightInPixels
    );
    this.physics.world.setBoundsCollision(true, true, true, true);
    this.map.setCollision(2);

    // Player
    const player = createPlayer({
      x: config.tiles.width * 3, // 20,
      y: config.tiles.height * 2, // 10,
      scene: this,
      maxSpeed: config.player.maxVelocity,
      dragMultiplier: 8,
    });
    this.player = player;
    this.player.on('die', () => {
      this.scene.run('death');
      this.physics.resume();
    });

    // Mobs
    const mobs = createMobs(this);

    // Exits
    const [{ x: exitX, y: exitY }] = getRandomSpawnPoints(this.map, 1);
    const exits = this.physics.add.group();
    exits.create(
      this.map.tileToWorldX(exitX) + this.map.tileWidth / 2,
      this.map.tileToWorldY(exitY) + this.map.tileHeight / 2,
      'door'
    );

    const floorLayer = this.map.getLayer('floor').tilemapLayer;
    this.physics.add.collider(player, floorLayer);

    const playerVsMobs = this.physics.add.collider(player, mobs);
    playerVsMobs.collideCallback = (
      _: GameObjects.GameObject,
      mob: GameObjects.GameObject
    ) => {
      const mobEntity = mob as Mob;
      if (
        isWithinFieldOfView(
          mobEntity.body.position,
          mobEntity.direction.bearing,
          mobEntity.sight.fieldOfViewRadians,
          this.player.body.position as Phaser.Math.Vector2
        )
      ) {
        this.player.die();
      } else {
        mobEntity.die();
        this.events.emit('mob.die', mobEntity);
      }
    };

    const playerVsExits = this.physics.add.collider(player, exits);
    playerVsExits.overlapOnly = true;
    playerVsExits.collideCallback = () => {
      this.scene.start('floor-cleared');
    };

    this.physics.add.collider(mobs, floorLayer);

    // Items
    this.items = this.physics.add.group({
      collideWorldBounds: true,
      // runChildUpdate: true,
    });

    // Add some dashes for me to pick up :D
    (() => {
      const dashSpawns = getRandomSpawnPoints(this.map, 10);
      dashSpawns.forEach((ds) => {
        // Item icon is smaller than a single tile and it's registration point
        // is in the center of the sprite so we need to add 16px to center it
        // on the map tile.
        this.items.add(createDash(this, 16 + ds.x * 32, 16 + ds.y * 32), true);
      });
    })();

    const playerVsItems = this.physics.add.collider(player, this.items);
    playerVsItems.overlapOnly = true;
    playerVsItems.collideCallback = (collector, itemGameObj) => {
      const player = collector as Player;
      const item = itemGameObj as Item;

      player.inventory.clear();
      player.inventory.add(item);

      const ui = this.scene.get('home-ui') as HomeUI;
      ui.addItem(this, item);
    };

    this.keys = {
      up: this.input.keyboard.addKey(KeyCodes.W),
      down: this.input.keyboard.addKey(KeyCodes.S),
      left: this.input.keyboard.addKey(KeyCodes.A),
      right: this.input.keyboard.addKey(KeyCodes.D),
      wait: this.input.keyboard.addKey(KeyCodes.SPACE),
      use: this.input.keyboard.addKey(KeyCodes.SHIFT),
      dash: this.input.keyboard.addKey(KeyCodes.ENTER),
    };

    this.cameras.main.startFollow(player);
    this.cameras.main.setZoom(3);
    this.cameras.main.setBounds(
      0,
      0,
      this.map.widthInPixels,
      this.map.heightInPixels
    );
  }

  movePlayerKeyboard(): boolean {
    // Use items
    if (this.keys.use.isDown) {
      if (this.player.inventory.size()) {
        const item = this.player.inventory.getItemAt(0);
        item.use();
      }
    }

    let x = 0,
      y = 0;

    if (this.keys.up.isDown) {
      y = -1;
    }
    if (this.keys.down.isDown) {
      y = 1;
    }
    if (this.keys.left.isDown) {
      x = -1;
    }
    if (this.keys.right.isDown) {
      x = 1;
    }
    const moved = x !== 0 || y !== 0;

    this.player.move(x, y);

    return moved;
  }

  movePlayerGamePad(): boolean {
    if (this.input.gamepad.total === 0) {
      return false;
    }

    const pad = this.input.gamepad.getPad(0);

    const minInput = 0.2;
    let x = pad.axes[0].getValue();
    let y = pad.axes[1].getValue();

    x = Math.abs(x) > minInput ? x : 0;
    y = Math.abs(y) > minInput ? y : 0;

    if (x || y) {
      this.player.move(x, y);
      return true;
    }

    return false;
  }

  waitKeyboard(): boolean {
    if (this.keys.wait.isDown) {
      this.player.wait();
      return true;
    }
    return false;
  }

  waitGamepad(): boolean {
    if (this.input.gamepad.total === 0) return false;

    const pad = this.input.gamepad.getPad(0);
    if (pad.isButtonDown(2)) {
      this.player.wait();
      return true;
    }

    // Use items.
    if (pad.isButtonDown(0) && this.player.inventory.size()) {
      this.player.inventory.getItemAt(0).use();
      return true;
    }

    return false;
  }

  update(time: number, delta: number) {
    if (!this.player.isAlive) {
      return;
    }

    // Perform pre-update actions on any applicable inventory items.
    this.player.inventory.preUpdate(time, delta);

    // Update the player's internal state.
    this.player.update(time, delta);

    const gotInput = this.movePlayerKeyboard() || this.movePlayerGamePad();
    const minMovement = 0.5;
    const moved =
      Math.abs(this.player.body.velocity.x) > minMovement ||
      Math.abs(this.player.body.velocity.y) > minMovement;
    const waited = this.waitKeyboard() || this.waitGamepad();

    // Perform post-update actions on any applicable inventory items.
    this.player.inventory.postUpdate(time, delta);

    if (moved || gotInput || waited) {
      this.physics.resume();
    } else {
      this.physics.pause();
      this.physics.world.update(time, delta / 100);
    }
  }
}
