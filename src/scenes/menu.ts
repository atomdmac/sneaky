import { gamepadButtonDown } from '@src/lib/gamepad';
import { Input } from 'phaser';

const {
  Keyboard: { KeyCodes },
} = Input;

type Keys = {
  restart: Phaser.Input.Keyboard.Key;
};

export default class Menu extends Phaser.Scene {
  declare keys: Keys;

  create() {
    const { width } = this.renderer;
    const top = 200;
    const centerX = width / 2;

    const title = this.add.text(0, 0, 'SNEAKY');
    title.setFontSize(100);
    title.setAlpha(0);
    title.setPosition(centerX - title.width / 2, top);

    this.tweens.add({
      targets: [title],
      alpha: 1,
      duration: 1000,
    });
    this.tweens.add({
      targets: [title],
      props: {
        y: title.y - 20,
      },
      duration: 2000,
      yoyo: true,
      repeat: -1,
      ease: Phaser.Math.Easing.Quadratic.InOut,
    });

    const instructions = this.add.text(
      0,
      0,
      `GAMEPLAY:
----------
1. Bad Guys only move when you do.
2. Try to find the exit door without being seen by the Bad Guys.
3. Collect items to aid in your escape.
4. Hit the Bad Guys while they can't see you to neutralize them.
5. When Bad Guys are nearby, you can "hear" them as radar blips`
    );
    instructions.setPosition(centerX - instructions.width / 2, top + 140);

    const controls = this.add.text(
      0,
      0,
      `CONTROLS:
----------
w/a/s/d: Move
Wait: Space
Shift: Use Item`
    );
    controls.setPosition(instructions.x, top + 300);

    const enterToContinue = this.add.text(0, 0, 'Press Enter to Start');
    enterToContinue.setPosition(
      centerX - enterToContinue.width / 2,
      controls.y + 150
    );

    this.keys = {
      restart: this.input.keyboard.addKey(KeyCodes.ENTER),
    };
  }

  restartGame() {
    this.scene.start('home');
  }

  update() {
    if (this.keys.restart.isDown || gamepadButtonDown(this, 1)) {
      this.restartGame();
    }
  }
}
