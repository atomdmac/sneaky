import { Player } from '@src/entities/player';
import { AudibleManager } from '@src/lib/audible';
import { AStarFinder } from 'astar-typescript';
import { Scene } from 'phaser';

export default interface SneakyScene<TKeys = Record<string, unknown>>
  extends Scene {
  map: Phaser.Tilemaps.Tilemap;
  player: Player;
  keys: TKeys;
  audible: AudibleManager;
  astar: AStarFinder;
}
