import { BisectionRoom } from '@src/maps/bisection-map';
import Phaser from 'phaser';

export default class Death extends Phaser.Scene {
  preload() {
    this.load.image('dirt_1', 'assets/dirt_1.png');
    this.load.image('stone_1', 'assets/stone_1.png');
  }

  create() {
    const bMap = new BisectionRoom({
      x: 0,
      y: 0,
      width: 30,
      height: 30,
      minWidth: 5,
      minHeight: 5,
      splitThreshold: 0.25,
    });
    bMap.split();
    bMap.generateDoors();
    // bMap.draw(this);

    const tileMap = this.add.tilemap('bisection-map', 32, 32, 30, 30);
    const dirt = tileMap.addTilesetImage(
      'dirt_1',
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      0
    );
    const stone = tileMap.addTilesetImage(
      'stone_1',
      undefined,
      undefined,
      undefined,
      undefined,
      undefined,
      1
    );
    const tiles = [dirt, stone];

    const terrain = tileMap.createBlankLayer('terrain', tiles);
    terrain.fill(0);
    bMap.populateTileMapLayer(terrain);

    this.cameras.main.setZoom(0.5);
  }
}
