import Item from '@src/entities/items/item';
import Phaser, { Scene } from 'phaser';

export default class HomeUI extends Phaser.Scene {
  public stats = {
    killCount: 0,
  };

  private declare text: {
    item: Phaser.GameObjects.Text;
    kills: Phaser.GameObjects.Text;
  };

  private currentItem: Item | null;

  constructor(...args: ConstructorParameters<typeof Phaser.Scene>) {
    super(...args);
    this.handleMobDie = this.handleMobDie.bind(this);
    this.currentItem = null;
  }

  handleMobDie() {
    this.stats.killCount += 1;
    this.text.kills.setText('KILLS: ' + this.stats.killCount);
  }

  addItem(previousScene: Scene, item: Item) {
    const camera = previousScene.cameras.main;

    const worldView = camera.worldView;
    const newX = (item.x - worldView.x) * camera.zoom;
    const newY = (item.y - worldView.y) * camera.zoom;
    item.setPosition(newX, newY);
    item.setScale(camera.zoom);

    this.add.existing(item);

    // TODO: Animate item being destroyed.
    if (this.currentItem) {
      this.tweens.add({
        targets: this.currentItem,
        props: {
          alpha: 0,
        },
        duration: 150,
        onComplete: (_, targets) => {
          targets.forEach((t) => t.destroy());
        },
      });
    }
    this.currentItem = item;

    // Move to top of screen.
    this.add.tween({
      targets: item,
      duration: 250,
      props: {
        x: 175,
        y: 28,
      },
      ease: Phaser.Math.Easing.Quadratic.InOut,
      onComplete: () => {
        item.durabilityDisplay.setVisible(true);
      },
    });

    // Enlarge to simulate "picking up"
    this.add.tween({
      targets: item,
      duration: 250 / 3,
      props: {
        scaleX: item.scaleX * 2,
        scaleY: item.scaleY * 2,
      },
      ease: Phaser.Math.Easing.Quadratic.InOut,
    });
    this.add.tween({
      targets: item,
      duration: 500,
      delay: 250 / 3,
      props: {
        scaleX: item.scaleX,
        scaleY: item.scaleY,
      },
      ease: Phaser.Math.Easing.Bounce.Out,
    });
  }

  create() {
    this.stats.killCount = 0;

    this.text = {
      item: this.add.text(0, 0, 'ITEM:', {
        fontSize: '5em',
      }),
      kills: this.add.text(256, 0, 'KILLS: ' + this.stats.killCount, {
        fontSize: '5em',
      }),
    };

    const main = this.scene.get('home');
    // HACK: Not totally sure why this listener needs to be removed to prevent duplicates.
    main.events.removeListener('mob.die', this.handleMobDie);
    main.events.on('mob.die', this.handleMobDie);
  }
}
