import { gamepadButtonDown } from '@src/lib/gamepad';
import { Input } from 'phaser';

const {
  Keyboard: { KeyCodes },
} = Input;

type Keys = {
  restart: Phaser.Input.Keyboard.Key;
};

export default class Death extends Phaser.Scene {
  public declare keys: Keys;

  create() {
    const { width, height } = this.renderer;

    const redOverlay = this.add.graphics();
    const blackOverlay = this.add.graphics();

    const color = 0xff0000;

    blackOverlay.fillStyle(0x000000);
    blackOverlay.fillRect(0, 0, width, height);

    redOverlay.fillStyle(color);
    redOverlay.fillRect(0, 0, width, height);

    const blackMessage = this.add.text(0, 0, 'You Have Died.');
    blackMessage.setFontSize(100);
    blackMessage.setAlign('center');
    blackMessage.setColor('#000000');
    blackMessage.setX(width / 2 - blackMessage.width / 2);
    blackMessage.setY(height / 2 - blackMessage.height / 2);

    const whiteMessage = this.add.text(0, 0, 'You Have Died.');
    whiteMessage.setFontSize(100);
    whiteMessage.setAlign('center');
    whiteMessage.setColor('#ffffff');
    whiteMessage.setX(width / 2 - whiteMessage.width / 2);
    whiteMessage.setY(height / 2 - whiteMessage.height / 2);
    whiteMessage.setDepth(100);

    blackMessage.setAlpha(0);
    whiteMessage.setAlpha(0);
    blackOverlay.setAlpha(0);
    redOverlay.setAlpha(0);

    this.tweens.add({
      targets: [blackMessage],
      alpha: 1,
      duration: 500,
    });
    this.tweens.add({
      targets: [redOverlay],
      alpha: 0.5,
      duration: 500,
    });

    this.tweens.add({
      targets: [blackOverlay],
      alpha: 1,
      delay: 5000,
      duration: 5000,
    });
    this.tweens.add({
      targets: [whiteMessage],
      props: {
        alpha: 1,
      },
      delay: 5000,
      duration: 5000,
    });

    this.keys = {
      restart: this.input.keyboard.addKey(KeyCodes.ENTER),
    };

    const homeScene = this.scene.get('home');
    homeScene.tweens.add({
      targets: homeScene.cameras.main,
      props: {
        zoom: 5,
      },
      ease: Phaser.Math.Easing.Quadratic.Out,
      duration: 1000,
    });
  }

  restartGame() {
    this.scene.start('home');
  }

  update() {
    if (this.keys.restart.isDown || gamepadButtonDown(this, 1)) {
      this.restartGame();
    }
  }
}
