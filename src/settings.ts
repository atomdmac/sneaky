export type GameSettings = {
  fogOfWar: boolean;
  width: number;
  height: number;
  fontSize: number;
  forceSquareRatio: boolean;
  hudPadding: number;
  bg?: string;
}

const defaultGameSettings: GameSettings = {
  fogOfWar: true,
  width: 30,
  height: 30,
  fontSize: 24,
  forceSquareRatio: true,
  hudPadding: 3
}

export default defaultGameSettings;
