import { Cell } from '../floor';
import { Entity } from '../entities';

export interface IModifier {
    id: string;
    value: number;
}

export interface IModdable {
  modifiers: IModifier[]
}

export interface IComponent {}

export type Modifier<C extends IComponent> = {
  id: string;
  field: keyof C;
  value: (val: C[keyof C], entity: Entity) => C[keyof C];
}

export abstract class AbstractComponent<C extends IComponent> implements IComponent {
  mods: Modifier<C>[] = [];
  entity: Entity;
  constructor (entity: Entity) {
    this.entity = entity;
  }
}

export class CollectableComponent extends AbstractComponent<CollectableComponent>{
  type: 'food' | 'money' = 'food';
  count: number = 1;
  belongsTo: Entity | null = null;
}

export class CollectorComponent extends AbstractComponent<CollectorComponent> {
  // TODO: Improve typings so that Entity _must_ have CollectableComponent
  collectables: Entity[] = [];
  collecting: Entity | null;
}

export interface IConsumption {
  collectable: CollectableComponent;
  delta: number;
}

export class ConsumerComponent extends AbstractComponent<ConsumerComponent> {
  consuming: IConsumption[] = [];
}

export class AliveComponent extends AbstractComponent<AliveComponent> {
  isAlive: boolean = true;
}

export class VisibleComponent extends AbstractComponent<VisibleComponent> {
  character: string = '@';
  foregroundColor: string = '#fff';
  backgroundColor: string = '#000';
}

export class PositionComponent extends AbstractComponent<PositionComponent> {
  x: number = 0;
  y: number = 0;
}

export class BearingComponent extends AbstractComponent<BearingComponent> {
  direction: number; // See ROT.DIRS in rot-js
}

export class NoiseComponent extends AbstractComponent<NoiseComponent> {
  noiseLevel: number = 0;
}

export class SpeedComponent extends AbstractComponent<SpeedComponent> implements IModdable {
  base: number = 1;
  modifiers: IModifier[] = [];
}

export class FieldOfViewComponent extends AbstractComponent<FieldOfViewComponent> {
  radius: number = 5;
  memory: WeakSet<Cell> = new WeakSet();
  current: Cell[] = [];
}

export class KeyboardControlComponent extends AbstractComponent<KeyboardControlComponent> {}

export class FollowPathComponent extends AbstractComponent<FollowPathComponent> {
  path: Cell[] = [];
}

export class ImpassableComponent extends AbstractComponent<ImpassableComponent> { }

export class DoorComponent extends AbstractComponent<DoorComponent> {}

export class DoorUserComponent extends AbstractComponent<DoorUserComponent> {
  using: DoorComponent | null = null;
}
