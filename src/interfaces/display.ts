export interface Display {
  clear: Function;
  computeFontSize: Function;
  computeSize: Function;
  DEBUG: Function;
  draw: Function;
  drawText: Function;
  eventToPosition: Function;
  getContainer: Function;
  getOptions: Function;
  setOptions: Function;
}

export interface DisplaySet {
  main: Display;
  effects: Display;
}
