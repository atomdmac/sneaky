import { Renderer } from '../renderer';
import { Game } from '../game';

export interface GameState {
  renderer: Renderer;
  game: Game;
  start: Function;
  stop: Function;
  update: Function;
  render: Function
}
