import Phaser from 'phaser';
import MenuScene from '@src/scenes/menu';
import HomeScene from '@src/scenes/home';
import HomeUIScene from '@src/scenes/home-ui';
import DeathScene from '@src/scenes/death';
import FloorClearedScene from '@src/scenes/floored-cleared';

export const DEPTHS = {
  map: 0,
  creatures: 1,
  fieldOfView: 2,
  audibles: 3,
};

export type SneakyConfig = Phaser.Types.Core.GameConfig & {
  tiles: {
    width: number;
    height: number;
  };
  player: {
    maxVelocity: number;
  };
};

const config: SneakyConfig = {
  type: Phaser.AUTO,
  parent: 'sneaky',
  width: 30 * 32,
  height: 30 * 32,
  zoom: 1,
  scene: [
    new MenuScene({ key: 'menu' }),
    new HomeScene({ key: 'home' }),
    new HomeUIScene({ key: 'home-ui' }),
    new DeathScene({ key: 'death' }),
    new FloorClearedScene({ key: 'floor-cleared' }),
  ],
  input: {
    keyboard: true,
    gamepad: true,
  },
  pixelArt: true,
  physics: {
    default: 'arcade',
    arcade: {
      isPaused: false,
      gravity: {
        x: 0,
        y: 0,
      },
      debug: false,
      debugShowBody: true,
    },
  },
  tiles: {
    width: 32,
    height: 32,
  },
  player: {
    maxVelocity: 120,
  },
};

export default config;
