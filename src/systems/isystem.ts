import { Game } from '../game';
import { Entity } from '../entities';

export interface ISystem {
  activate: (game: Game) => void;
  deactivate: (game: Game) => void;
  update: (target: Entity, game: Game) => void
}

