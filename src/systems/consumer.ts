import { ISystem } from './isystem';
import { ConsumerComponent, CollectorComponent, CollectableComponent, IConsumption } from '../components';
import { Entity } from '../entities';

export function startConsuming (
  consumer: Entity,
  collectable: Entity,
  delta: number
) {
  const consumerCmpt = consumer.getComponent(ConsumerComponent);
  const collectableCmpt = collectable.getComponent(CollectableComponent);

  if (consumerCmpt && collectableCmpt) {
    consumerCmpt.consuming.push({
      collectable: collectableCmpt,
      delta
    });
  }
}

export function stopConsuming (
  consumer: Entity,
  collectable: Entity,
) {
  const consumerCmpt = consumer.getComponent(ConsumerComponent);
  const collectableCmpt = collectable.getComponent(CollectableComponent);
  if (consumerCmpt && collectableCmpt) {
    consumerCmpt.consuming = consumerCmpt.consuming.filter(
      (c: IConsumption) => c.collectable !== collectableCmpt
    )
  }
}


export class ConsumerSystem implements ISystem {
  activate () {}
  deactivate () {}

  update (current: Entity) {
    const consumerCmpt = current.getComponent(ConsumerComponent);
    const collectorCmpt = current.getComponent(CollectorComponent);

    if (!consumerCmpt || !collectorCmpt) {
      return;
    }

    consumerCmpt.consuming.forEach((modifier: IConsumption) => {
      const collectable = collectorCmpt.collectables.find((c: Entity) =>
        c.getComponent(CollectableComponent) === modifier.collectable
      );
      if (collectable) {
        const collectableCmpt = collectable.getComponent(CollectableComponent);
        collectableCmpt.count += modifier.delta;
      }
    });
  }
}
