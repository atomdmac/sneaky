import { Game } from '../game';
import { Cell, Floor } from '../floor';
import { BearingComponent, FollowPathComponent, ImpassableComponent, PositionComponent } from '../components';
import { Entity } from '../entities';
import { ISystem } from './isystem';
import { getEntitiesAt } from '../entities/utils';
import { head, tail } from 'lodash';
import { Path } from 'rot-js';
import { getDirectionBetween } from '../helpers/directions';

type Direction = 'NORTH' 
  | 'NORTH_EAST' 
  | 'EAST' 
  | 'SOUTH_EAST' 
  | 'SOUTH' 
  | 'SOUTH_WEST' 
  | 'WEST' 
  | 'NORTH_WEST' ;

export const DIRECTIONS: [Direction, number, number][] = [
  ['NORTH', 0, -1],
  ['NORTH_WEST', -1, -1],
  ['NORTH_EAST', 1, -1],
  ['SOUTH', 0, 1],
  ['SOUTH_WEST', -1, 1],
  ['SOUTH_EAST', 1, 1],
  ['WEST', -1, 0],
  ['EAST', 1, 0]
];

export function moveTo(
  entity: Entity,
  direction: Direction,
  map: Floor
): boolean {
  const path = entity.getComponent(FollowPathComponent).path
  const nextCell = getAdjacentCell(entity, direction, map);
  if (nextCell && nextCell.passable) {
    path.push(nextCell);
    return true;
  }
  return false;
}

export function getAdjacentCell(
  entity: Entity,
  direction: Direction,
  map: Floor
): Cell | null {
  const pos = entity.getComponent(PositionComponent);
  const nextPos = DIRECTIONS.find(([dir]) => dir === direction);
  return map.getCell(
    pos.x + nextPos[1],
    pos.y + nextPos[2]
  );
}

export function getPathBetween (
  floor: Floor,
  x1: number,
  y1: number,
  x2: number,
  y2: number
) {
  const passableCallback = (x: number, y: number) => {
    const cell: Cell = floor.getCell(x, y);
    if (cell && cell.passable) {
      return true;
    } else {
      return false;
    }
  }
  const aStar = new Path.AStar(x1, y1, passableCallback);
  const path: Cell[] = [];
  aStar.compute(x2, y2, (x, y) => {
    path.push(floor.getCell(x, y));
  });
  
  // The first item in the path is the starting point.  We can get rid of it.
  path.shift();

  return path;
}

export class FollowPathSystem implements ISystem {
  activate () {}
  deactivate () {}
  update (current: Entity, game: Game) {
    if (current.hasComponent(FollowPathComponent)) {
      const fp = current.getComponent(FollowPathComponent);
      if (fp.path.length) {
        const nextPos = head(fp.path);
        fp.path = tail(fp.path);

        if (current.hasComponent(PositionComponent)) {
          // Don't move thru other impassable entities.
          const isValidMove = !getEntitiesAt(
            game.world.entities, nextPos.x, nextPos.y,
            [ImpassableComponent]
          ).length

          if (isValidMove) {
            const pos = current.getComponent(PositionComponent);

            if (current.hasComponent(BearingComponent)) {
              const bearing = current.getComponent(BearingComponent);
              bearing.direction = getDirectionBetween(pos.x, pos.y, nextPos.x, nextPos.y);
            }

            pos.x = nextPos.x;
            pos.y = nextPos.y;

          }
        }
      }
    }
  }
}
