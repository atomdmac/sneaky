import { ISystem } from './isystem';
import { CollectableComponent, CollectorComponent } from '../components';
import { Entity } from '../entities';
import {Game} from '../game';

export function canCollect(collector: Entity, collectable: Entity): boolean {
  const collectorCmpt = collector.getComponent(CollectorComponent);
  const collectableCmpt = collectable.getComponent(CollectableComponent);
  return collectorCmpt && collectableCmpt && collectableCmpt.belongsTo === null;
}

export function getCollectable(collector: Entity, type: string): Entity | null {
  const collectorCmpt = collector.getComponent(CollectorComponent);
  if (!collectorCmpt) {
    return null;
  }
  return collectorCmpt.collectables.find((collectable: Entity) => {
    const collectableCmpt = collectable.getComponent(CollectableComponent)
    return collectableCmpt.type === type;
  });
}

export function collect(collector: Entity, collectable: Entity) {
  const collectorCmpt = collector.getComponent(CollectorComponent);
  const collectableCmpt = collectable.getComponent(CollectableComponent);
  if (!collectorCmpt) {
    throw new Error('Entity does not have CollectorComponent');
  }
  if (!collectableCmpt) {
    throw new Error('Entity does not have CollectableComponent');
  }
  const existingCollectable = getCollectable(collector, collectableCmpt.type);
  if (existingCollectable) {
    const existingCollectableCmpt = existingCollectable.getComponent(CollectableComponent);
    existingCollectableCmpt.count += collectableCmpt.count;
  } else {
    collectorCmpt.collectables.push(collectable);
  }
}

export function updateCollectableCount(collector: Entity, type: string, diff: number) {
  const collectable = getCollectable(collector, type);
  if (collectable) {
    const collectableCmpt = collectable.getComponent(CollectableComponent);
    collectableCmpt.count += diff;
  }
}

export function getCollectableCount(collector: Entity, type: string): number {
  const collectable = getCollectable(collector, type);
  if (collectable) {
    const collectableCmpt = collectable.getComponent(CollectableComponent);
    return collectableCmpt.count;
  }
  return 0;
}

export class CollectorSystem implements ISystem {
  activate () {}
  deactivate () {}

  update (current: Entity, game: Game) {
    const collector = current.getComponent(CollectorComponent);
    if (!collector || !collector.collecting) {
      return;
    }

    collect(current, collector.collecting);
    game.world.markEntityPurgable(collector.collecting);

    collector.collecting = null;
  }
}
