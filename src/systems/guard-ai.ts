import { AliveComponent, FollowPathComponent, PositionComponent, VisibleComponent} from '../components';
import { Entity } from '../entities';
import { GuardAIComponent } from '../entities/guard';
import { Game } from '../game';
import { ISystem } from './isystem';
import { canSee } from './field-of-view';
import { getPathBetween } from './follow-path';
import { areAdjacent } from '../helpers/directions';
import {canHear} from './field-of-hearing';

export class GuardAISystem implements ISystem {
  activate () {}
  deactivate () {}

  update (current: Entity, game: Game) {
    if (current.hasComponent(GuardAIComponent)) {
      const player = game.world.player;
      const path = current.getComponent(FollowPathComponent);
      const pos = current.getComponent(PositionComponent);
      const vis = current.getComponent(VisibleComponent);
      const playerPos = player.getComponent(PositionComponent);

      const sees = canSee(current, playerPos.x, playerPos.y, game.world.floor);
      const hears = canHear(current, player);
      if (sees || hears) {
        // If we're next to the player, nab 'em!
        if (areAdjacent(pos.x, pos.y, playerPos.x, playerPos.y)) {
          const alive = game.world.player.getComponent(AliveComponent);
          alive.isAlive = false;
        }

        // Update destination
        path.path = getPathBetween(
          game.world.floor,
          playerPos.x,
          playerPos.y,
          pos.x,
          pos.y
        );
        // Update demeanor
        vis.backgroundColor = '#ff0000';
      }

      else if (path.path.length === 0) {
        // If I'm no longer heading somewhere, choose a random place to go.
        const randomPos = game.world.floor.getRandomSpawn();
        path.path = getPathBetween(
          game.world.floor,
          randomPos.x,
          randomPos.y,
          pos.x,
          pos.y
        );
        vis.backgroundColor = '#000';
      }
    }
  }
}
