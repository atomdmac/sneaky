import {AbstractComponent, PositionComponent} from '../components';
import {Entity, getDistanceBetween} from '../entities';
import {Game} from '../game';
import { ISystem } from './isystem';

export class FieldOfSoundComponent extends AbstractComponent<FieldOfSoundComponent> {
  description: string = 'a sound';
  radius: number = 0;
}

export class FieldOfHearingComponent extends AbstractComponent<FieldOfHearingComponent> {
  radius: number = 0;
  current: Entity[] = [];
}

export function canHear (current: Entity, target: Entity): boolean {
  const hearing = current.getComponentValue(FieldOfHearingComponent, 'current');
  return hearing.some((entity: Entity) => entity === target);
}

export class FieldOfHearingSystem implements ISystem {
  activate () {}
  deactivate () {}

  update (current: Entity, game: Game) {
    const meetsReqs = current.hasAllComponents([
      FieldOfHearingComponent,
      PositionComponent
    ]);

    if (meetsReqs) {
      const hearing = current.getComponent(FieldOfHearingComponent);
      const hearingRadius = current.getComponentValue(FieldOfHearingComponent, 'radius');
      const pos = current.getComponent(PositionComponent);

      const newHearing = game.world.entities.filter((entity: Entity) => {
        const targetMeetsReqs = entity.hasAllComponents([
          PositionComponent,
          FieldOfSoundComponent
        ]);

        if (!targetMeetsReqs) return false;

        const entityPos = entity.getComponent(PositionComponent);
        const soundRadius = entity.getComponentValue(FieldOfSoundComponent, 'radius');

        const distanceBetween = getDistanceBetween(pos.x, pos.y, entityPos.x, entityPos.y);
        const overlap = distanceBetween < soundRadius + hearingRadius;

        return soundRadius > 0 && overlap;
      });
      hearing.current = newHearing;
    }
  }
}
