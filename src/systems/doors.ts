import {DoorUserComponent} from '../components';
import {Entity} from '../entities';
import {Game} from '../game';
import { ISystem } from './isystem';

export class DoorsSystem implements ISystem {
  activate () {}
  deactivate () {}

  update (entity: Entity, game: Game) {
    if (entity.hasComponent(DoorUserComponent)) {
      const doorUserComponent = entity.getComponent(DoorUserComponent);
      if (doorUserComponent.using) {
        doorUserComponent.using = null;
        game.world.nextFloor();
      }
    }
  }
}
