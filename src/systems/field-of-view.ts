import { Game } from '../game';
import { Floor } from '../floor';
import { ISystem } from './isystem';
import { BearingComponent, FieldOfViewComponent, PositionComponent } from '../components';
import { Entity } from '../entities';
import { Cell } from '../floor';
import { FOV } from 'rot-js';

export function canSee (entity: Entity, x: number, y: number, floor: Floor): boolean {
  const fov = entity.getComponent(FieldOfViewComponent);
  const cell = floor.getCell(x, y);
  return !!fov.current.find((c: Cell) => c === cell);
}

export function couldSee (entity: Entity, x: number, y: number, floor: Floor): boolean {
  const fov = entity.getComponent(FieldOfViewComponent);
  const cell = floor.getCell(x, y);
  return fov.memory.has(cell);
}

export class FieldOfViewSystem implements ISystem {
  activate () {}

  deactivate () {}

  getVisibleCells(
    fov: any,
    floor: Floor,
    x: number,
    y: number,
    direction: number,
    radius: number = 10,
  ): Cell[] {
    const visibleCells: Cell[] = [];

    const getVisibility = (x: number, y: number) => {
      visibleCells.push(
        floor.getCell(x, y)
      );
    }

    if (typeof direction === 'number') {
      fov.compute90(x, y, radius, direction, getVisibility);
    } else {
      fov.compute(x, y, radius, getVisibility);
    }

    return visibleCells;
  }

  update (currentEntity: Entity, game: Game) {
    if (currentEntity.hasComponent(FieldOfViewComponent)) {
      // TODO: Do we have to create FOV objects on each tick?
      const fovGen = new FOV.RecursiveShadowcasting((x: number, y: number) => {
        const cell: Cell = game.world.floor.getCell(x, y);
        if (cell && cell.passable) {
          return true;
        } else {
          return false;
        }
      });

      const fov = currentEntity.getComponent(FieldOfViewComponent);
      const pos = currentEntity.getComponent(PositionComponent);
      const bearing = currentEntity.getComponent(BearingComponent);
      const currentFov = this.getVisibleCells(
        fovGen,
        game.world.floor,
        pos.x,
        pos.y,
        bearing && bearing.direction,
        fov.radius
      );

      fov.current = [];

      currentFov.forEach((cell) => {
        fov.current.push(cell);
        fov.memory.add(cell);
      });
    }
  }
}
