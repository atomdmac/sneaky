export class Keyboard {
  private currentlyPressed: any;

  constructor () {
    this.currentlyPressed = {};

    // HACK: Scope callbacks to this instance.
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
  }

  activate () {
    document.addEventListener('keydown', this.onKeyDown);
    document.addEventListener('keyup', this.onKeyUp);
  }

  deactivate () {
    document.removeEventListener('keydown', this.onKeyDown);
    document.removeEventListener('keyup', this.onKeyUp);
  }

  private getCharFromEvent(event: any): string {
    return event.key || String.fromCharCode(event.keyCode).toLowerCase();
  }

  private onKeyDown (event: any) {
    event.preventDefault();
    this.currentlyPressed[this.getCharFromEvent(event)] = true;
  }

  private onKeyUp (event: any) {
    delete this.currentlyPressed[this.getCharFromEvent(event)];
  }

  keyIsDown (character) {
    return !!this.currentlyPressed[character];
  }

  getAllPressed (): string[] {
    return Object.keys[this.currentlyPressed];
  }
}
