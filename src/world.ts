import { Cell, Floor } from './floor';
import { Game } from './game';
import { PositionComponent } from './components';
import { Entity, Door, Player } from './entities';
import {Guard} from './entities/guard';
import {Food} from './entities/food';
import {Money} from './entities/money';

class Stats {
  loot: number = 0;
  floorsClimbed: number = 0;

  public reset () {
    this.loot = 0;
    this.floorsClimbed = 0;
  }
}

export class World {
  game: Game;
  floor: Floor;
  player: Player;
  entities: Entity[];
  purgableEntities: Entity[];
  stats: Stats;

  width: number;
  height: number;

  constructor (game: Game, width: number = 30, height: number = 30) {
    this.game = game;
    this.width = width;
    this.height = height;
    this.reset();
  }

  reset () {
    this.player = new Player();
    this.stats = new Stats();
  }

  nextFloor () {
    // Get rid of everything from the previous floor.
    this.game.scheduler.clear();
    this.entities = [];
    this.purgableEntities = [];

    // Save stats
    this.stats.floorsClimbed += 1;

    // Climb up a floor.
    this.floor = new Floor(this.width, this.height);

    // Orient the player.
    const playerSpawn: Cell = this.floor.spawns.pop();

    const playerPos = this.player.getComponent(PositionComponent);
    playerPos.x = playerSpawn.x;
    playerPos.y = playerSpawn.y;
    this.entities.push(this.player);

    this.game.scheduler.add(this.player, true);

    // Add doors
    const doorSpawn = this.floor.spawns.shift();
    const downDoor = new Door();
    const downDoorPos = downDoor.getComponent(PositionComponent);
    downDoorPos.x = doorSpawn.x;
    downDoorPos.y = doorSpawn.y;
    this.entities.push(downDoor);

    const guards = this.generateGuards();
    guards.forEach((g) => {
      this.game.scheduler.add(g, true)
      this.entities.push(g);
    });

    const foods = this.generateFoods();
    foods.forEach((f) => {
      this.game.scheduler.add(f, true)
      this.entities.push(f);
    });

    const moneys = this.generateMoneys();
    moneys.forEach((m) => {
      this.game.scheduler.add(m, true)
      this.entities.push(m);
    });

    this.game.scheduler.next();
  }

  generateFoods (count: number = 3) {
    const foods = [];
    for (let f = 0; f < count; f++) {
      const food = new Food();
      const pos = food.getComponent(PositionComponent);
      const spawn = this.game.world.floor.getRandomSpawn();
      pos.x = spawn.x;
      pos.y = spawn.y;
      foods.push(food);
    }
    return foods;
  }

  generateGuards (count: number = 3) {
    const guards = [];
    for (let g = 0; g < count; g++) {
      const guard = new Guard();
      const pos = guard.getComponent(PositionComponent);
      const spawn = this.game.world.floor.getRandomSpawn();
      pos.x = spawn.x;
      pos.y = spawn.y;
      guards.push(guard);
    }
    return guards;
  }

  generateMoneys (count: number = 3) {
    const moneys = [];
    for (let m = 0; m < count; m++) {
      const money = new Money();
      const pos = money.getComponent(PositionComponent);
      const spawn = this.game.world.floor.getRandomSpawn();
      pos.x = spawn.x;
      pos.y = spawn.y;
      moneys.push(money);
    }
    return moneys;
  }

  markEntityPurgable (entity: Entity) {
    this.purgableEntities.push(entity);
  }

  // TODO: Can purgeMarkedEntities edit arrays in place?
  purgeMarkedEntities () {
    if (this.purgableEntities.length) {
      this.entities = this.entities.filter((entity: Entity) =>
        this.purgableEntities.find(
          (purgable: Entity) => entity !== purgable
        )
      );
      this.purgableEntities = [];
    }
  }
}
